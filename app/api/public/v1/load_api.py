from flask import Blueprint
from flask_restx import Api

from app.api.public.v1.namespaces.test import namespace as test_namespace
from app.api.public.v1.namespaces.metadata import namespace as metadata_namespace


def load_api_namespaces():
    bp = Blueprint('public_api', 'public_api')
    api = Api(bp)
    api.add_namespace(test_namespace)
    api.add_namespace(metadata_namespace)

    return bp
