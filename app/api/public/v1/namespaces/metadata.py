import logging

from flask_jwt_extended import jwt_required, get_jwt_identity

from app.api_authorizations import authorizations
from flask_restx import Namespace, Resource, reqparse

from app.services.metadata_service import get_resource

logger = logging.getLogger("webapp.api")

namespace = Namespace('metadata', description='Metadata related operations', authorizations=authorizations,
                      security=['Bearer Token'])


@namespace.doc(security=['Bearer Token'])
@namespace.route('/')
class MetadataEndpoint(Resource):

    method_decorators = [jwt_required]

    @namespace.doc('get_metadata')
    @namespace.doc(params={'resourceType': 'The type of the resource to be retrieved'})
    @namespace.doc(params={'resourceName': 'The name of the resource to be retrieved'})
    @namespace.doc(params={'parentType': 'The type of the parent resource to be retrieved'})
    @namespace.doc(params={'parentName': 'The name of the parent resource to be retrieved'})
    def get(self):
        """
        Get all the metadata matching the parameters

        #### Available resource types
            - codec
            - video_system
            - controller
            - loan
            - part
            - pc
            - projector
            - room

        ### Examples

        #### Example: Retrieve all the rooms
        ```
        https://av-dashboard-qa.web.cern.ch/api/v1/metadata?resourceType=room
        ```

        #### Example: Retrieve all the projectors from the room 13/2-005

        ```
        https://av-dashboard-qa.web.cern.ch/api/v1/metadata?resourceType=projector&parentName=13/2-005&parentType=room
        ```

        Note: parentType and parentName are both required.

        #### Example: Get the device's information by name and type

        ```
        https://av-dashboard-qa.web.cern.ch/api/v1/metadata?resourceType=codec&resourceName=VCROOM047
        ```

        Returned resources in this case will be the ones whose name starts with 'VCROOM047'


        """
        logger.info("Obtaining metadata from public API")
        current_user = get_jwt_identity()
        logger.info("Obtaining metadata from public API. User: {}".format(current_user))

        parser = reqparse.RequestParser()
        parser.add_argument('parentName', location='args')
        parser.add_argument('parentType', location='args')
        parser.add_argument('resourceName', location='args')
        parser.add_argument('resourceType', location='args', required=True)
        args = parser.parse_args()

        if not args.get("resourceType", None):
            return 'A resource type is required', 400

        result = get_resource(parent_type=args.get("parentType", None),
                              parent_name=args.get("parentName", None),
                              resource_type=args.get("resourceType", None),
                              resource_name=args.get("resourceName", None))

        return result
