import logging

from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restx import Namespace, Resource, fields

from app.api_authorizations import authorizations

logger = logging.getLogger('webapp.api.services')

namespace = Namespace('test', description='Test related operations', authorizations=authorizations,
                      security=['Bearer Token'])

test_model = namespace.model('TestModel', {
    'result': fields.String(required=True, description='The test result'),
})


@namespace.doc()
@namespace.route('/')
class TestEndpoint(Resource):
    @namespace.doc('test_endpoint')
    @namespace.marshal_with(test_model)
    def get(self):
        """
        Test response

         Example:
         ```
         https://av-dashboard-qa.web.cern.ch/api/v1/test/
         ```
        """
        logger.info("Obtaining test from public API (unprotected)")
        current_user = get_jwt_identity()
        logger.info("Obtaining test from public API (unprotected). User: {}".format(current_user))

        result = {'result': 'ok'}
        return result


@namespace.doc(security=['Bearer Token'])
@namespace.route('/secure')
class TestSecureEndpoint(Resource):
    decorators = [jwt_required]

    @namespace.doc('test_endpoint_secure')
    @namespace.marshal_with(test_model)
    def get(self):
        """
         Test response

         Example:
         ```
         https://av-dashboard-qa.web.cern.ch/api/v1/test/secure
         ```



         """
        logger.info("Obtaining test from public API (protected)")
        current_user = get_jwt_identity()
        logger.info("Obtaining test from public API (protected). User: {}".format(current_user))
        result = {'result': 'ok'}

        return result
