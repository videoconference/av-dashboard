from flask import g

from app.api.decorators import require_token
from app.api_authorizations import authorizations
from flask_restx import Namespace, Resource, fields

namespace = Namespace('users', description='Users related operations', authorizations=authorizations,
                      security=['Bearer Token'])


def allow_anonymous(fn):
    fn._allow_anonymous = True
    return fn


# @api.errorhandler(UnprocessableEntity)
# def _handle_webargs_error(exc):
#     data = getattr(exc, 'data', None)
#     if data and 'messages' in data:
#         return jsonify(error='invalid_args', messages=data['messages']), exc.code
#     return jsonify(error=exc.description), exc.code


# @api.route('/ping')
# @allow_anonymous
# def ping():
#     # dummy endpoint that can be used to check if the app is running
#     return '', 204


me_model = namespace.model('MeModel', {
    'email': fields.String,
    'first_name': fields.String,
    'last_name': fields.String,
    'uid': fields.String,
    'roles': fields.List(fields.String)
})


@namespace.doc(security=['Bearer Token'])
@namespace.route('/me')
class MeEndpoint(Resource):
    @namespace.doc('me')
    @namespace.marshal_with(me_model)
    @require_token
    def get(self):
        user = g.user
        if user:
            return g.user
        else:
            return "No user set", 401
