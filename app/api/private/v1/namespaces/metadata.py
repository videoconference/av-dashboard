import logging

from flask import g, request

from app.api.decorators import require_token, require_admin_token
from app.api_authorizations import authorizations
from flask_restx import Namespace, Resource, reqparse

from app.services.metadata_service import get_resource, set_resource

logger = logging.getLogger("webapp.api")

namespace = Namespace('metadata', description='Metadata related operations', authorizations=authorizations,
                      security=['Bearer Token'])


@namespace.doc(security=['Bearer Token'])
@namespace.route('/')
class MetadataEndpoint(Resource):
    @namespace.doc('get_metadata')
    @require_token
    def get(self):
        logger.info("Obtaining metadata from API")

        parser = reqparse.RequestParser()
        parser.add_argument('parentName', location='args')
        parser.add_argument('parentType', location='args')
        parser.add_argument('resourceName', location='args')
        parser.add_argument('resourceType', location='args')
        args = parser.parse_args()

        logger.info(args)

        logger.info("End of parameters")

        user = g.user
        public_resources = ['room', 'loan']
        admin_roles = ['admins', 'superadmins']
        # # # We only allow certain resources to admins
        if not args.get("resourceType", None) in public_resources and not any(x in admin_roles for x in user["roles"]):
            return '', 401

        return get_resource(parent_type=args.get("parentType", None),
                            parent_name=args.get("parentName", None),
                            resource_type=args.get("resourceType", None),
                            resource_name=args.get("resourceName", None))

    @require_admin_token
    @namespace.doc('set_metadata')
    def put(self):
        logger.info("Setting metadata from API")

        args = request.json
        payload = args

        parser = reqparse.RequestParser()
        parser.add_argument('parentName')
        parser.add_argument('parentType')
        parser.add_argument('resourceName')
        parser.add_argument('resourceType', type=str)
        args = parser.parse_args()

        return set_resource(parent_type=args.get("parentType", None),
                            parent_name=args.get("parentName", None),
                            resource_type=args.get("resourceType", None),
                            resource_name=args.get("resourceName", None),
                            new_data=payload)
