import base64
import json
import logging

from flask import request, g

from app.api.decorators import require_token, require_admin_token
from app.api_authorizations import authorizations
from flask_restx import Namespace, Resource, reqparse

from app.services.infoream_service import InforeamConnector

logger = logging.getLogger("webapp.api")

namespace = Namespace('stats', description='Stats related operations', authorizations=authorizations,
                      security=['Bearer Token'])


@namespace.doc(security=['Bearer Token'])
@namespace.route('/')
class StatsEndpoint(Resource):

    @namespace.doc('get_stats')
    @require_admin_token
    def get(self):
        logger.info("Obtaining stats from API")

        meeting_rooms = InforeamConnector().get_number_of_meeting_rooms()
        meeting_rooms_managed = InforeamConnector().get_number_of_meeting_rooms_managed()
        equipment_manufacturer = InforeamConnector().get_number_of_equipment_with_manufacturer()
        equipment_inventiored = InforeamConnector().get_number_of_equipment_inventiored()
        equipment_serial_number = InforeamConnector().get_number_of_equipment_with_serial_number()

        return {
            "data": {
                "meetingRooms": meeting_rooms,
                "meetingRoomsManaged": meeting_rooms_managed,
                "equipmentInventiored": equipment_inventiored,
                "equipmentWithManufacturer": equipment_manufacturer,
                "equipmentWithSerialNumber": equipment_serial_number
            }
        }
