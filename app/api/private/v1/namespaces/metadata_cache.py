import logging

from flask import g

from app.api.decorators import require_admin_token
from app.api_authorizations import authorizations
from flask_restx import Namespace, Resource

from app.extensions import cache

logger = logging.getLogger("webapp.api")

namespace = Namespace('metadata-cache', description='Metadata Cache related operations', authorizations=authorizations,
                      security=['Bearer Token'])


@namespace.doc(security=['Bearer Token'])
@namespace.route('/')
class MetadataCacheEndpoint(Resource):
    @namespace.doc('clear_cache')
    @require_admin_token
    def post(self):
        try:
            logger.info("Clearing metadata cache")
            user = g.user
            logger.info("Clearing metadata cache. User {} {}".format(user['first_name'], user['last_name']))
            cache.clear()
            return {'result': 'OK', 'message': 'The application cache has been cleared'}
        except Exception as e:
            logger.error(e)
            return {'result': 'ERROR', 'message': 'Unable to clean the application cache'}
