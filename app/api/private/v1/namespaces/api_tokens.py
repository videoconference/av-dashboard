import logging

from app.api.decorators import require_admin_token
from app.api_authorizations import authorizations
from flask_restx import Namespace, Resource, fields, abort

from app.daos.api_tokens import ApiTokenDAO, ApiTokenException

logger = logging.getLogger('webapp.api.services')

namespace = Namespace('api-keys', description='API Tokens related operations', authorizations=authorizations,
                      security=['Bearer Token'])


api_token_model = namespace.model('ApiTokenModel', {
    'id': fields.String(readOnly=True, required=True, description='The service unique identifier', attribute="id"),
    'name': fields.String(required=True, description='The service name', attribute="name"),
    'revoked': fields.Boolean(description='The service name', attribute="revoked"),
    'accessToken': fields.String(description='The service description', attribute='access_token'),
})


@namespace.doc(security=['Bearer Token'])
@namespace.route('/')
class ApiTokenListEndpoint(Resource):
    decorators = [require_admin_token]

    @namespace.doc('list_api_tokens')
    @namespace.marshal_list_with(api_token_model)
    def get(self):
        """ Get all tokens """
        tokens = ApiTokenDAO.get_all()

        return tokens

    @namespace.doc('create_api_token')
    @namespace.expect(api_token_model, description="The room model that is expected to receive")
    @namespace.marshal_with(api_token_model, code=201)
    def post(self):
        """ Create a new api token """
        args = namespace.payload

        try:
            room = ApiTokenDAO.create(args, commit=True)
            return room, 201
        except ApiTokenException as e:
            logger.error(e)
            abort(400, error=str(e))

        abort(400)


@namespace.doc(security=['Bearer Token'])
@namespace.route('/<string:token_id>')
class ApiTokenDetailsEndpoint(Resource):
    decorators = [require_admin_token]

    @namespace.doc('token_details')
    @namespace.marshal_with(api_token_model)
    def get(self, token_id):
        """ Get a token by its id """
        token = ApiTokenDAO.get_by_id(token_id)

        return token

    @namespace.doc('token_delete')
    @namespace.marshal_with(api_token_model)
    def delete(self, token_id):
        """ Revoke a token by id """
        logger.debug("Removing service status")
        result = ApiTokenDAO.delete_by_id(token_id)

        if result is True:
            return result, 204
        else:
            return result, 501
