import logging

from flask import g, request

from app.api.decorators import require_admin_token
from app.api_authorizations import authorizations
from flask_restx import Namespace, Resource, reqparse

from app.utils.infoream_utils import fetch_infoream_rooms, fetch_infoream_codecs, fetch_infoream_controllers, \
    fetch_infoream_loans, fetch_infoream_parts, fetch_infoream_pcs, fetch_infoream_projectors, fetch_infoream_video_systems

logger = logging.getLogger("webapp.api")

namespace = Namespace('infoream-fetch', description='Infoream Fetch related operations', authorizations=authorizations,
                      security=['Bearer Token'])


@namespace.doc(security=['Bearer Token'])
@namespace.route('/')
class InforeamFetchEndpoint(Resource):
    @namespace.doc('infoream_fetch')
    @require_admin_token
    def post(self):
        try:
            payload = request.json

            logger.info(payload)

            def fetch_resource(argument):
                switcher = {
                    'codec': fetch_infoream_codecs,
                    'controller': fetch_infoream_controllers,
                    'video_system': fetch_infoream_video_systems,
                    'loan': fetch_infoream_loans,
                    'part': fetch_infoream_parts,
                    'pc': fetch_infoream_pcs,
                    'projector': fetch_infoream_projectors,
                    'room': fetch_infoream_rooms
                }
                # Get the function from switcher dictionary
                func = switcher.get(argument, lambda: "Invalid resource")
                # Execute the function
                func()

            logger.info("Fetching from Infoream API Call")
            user = g.user
            logger.info("Fetching from Infoream API. User {} {}".format(user['first_name'], user['last_name']))
            fetch_resource(payload['resourceType'])
            return {'result': 'OK', 'message': 'The application cache has been cleared'}
        except Exception as e:
            logger.error("ERROR Fetching from Infoream API Call")
            logger.error(e)
            return {'result': 'ERROR', 'message': 'Unable to clean the application cache'}
