from flask import Blueprint
from flask_restx import Api

from app.api.private.v1.namespaces.users import namespace as users_namespace
from app.api.private.v1.namespaces.metadata import namespace as metadata_namespace
from app.api.private.v1.namespaces.metadata_cache import namespace as metadata_cache_namespace
from app.api.private.v1.namespaces.stats import namespace as stats_namespace
from app.api.private.v1.namespaces.infoream_fetch import namespace as infoream_fetch_namespace
from app.api.private.v1.namespaces.api_tokens import namespace as api_tokens_namespace


def load_api_namespaces():
    bp = Blueprint('api', __name__)
    api = Api(bp)
    api.add_namespace(users_namespace)
    api.add_namespace(metadata_namespace)
    api.add_namespace(metadata_cache_namespace)
    api.add_namespace(stats_namespace)
    api.add_namespace(infoream_fetch_namespace)
    api.add_namespace(api_tokens_namespace)

    return bp
