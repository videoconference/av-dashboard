import datetime
import logging
import os

from anytree import AnyNode
from anytree.exporter import JsonExporter

from app.services.gitlab_service import GitlabClient
from app.services.infoream_service import InforeamConnector
from secret.config import GITLAB_ACCESS_TOKEN, BRANCH

logger = logging.getLogger("job.infoream_utils")


def generate_rooms_file(rooms_query_results):

    def format_room_name_infoream(room_name):
        return room_name.replace('CCA-', '').replace('.', '/', 1).replace('.', '-', 1)

    logger.info("Generating rooms file...")
    rooms = AnyNode(name="rooms", type="list")

    for p in rooms_query_results:
        AnyNode(
            name=format_room_name_infoream(p[0]),
            infoream_name=p[0],
            type="room",
            parent=rooms,
            source="infoream",
            description=p[1],
            status=p[2],
            commissioned=p[3].strftime("%Y-%m-%d") if p[3] else None,
            sponsor=p[4],
            comments=p[5],
            expected_date=p[6].strftime("%Y-%m-%d") if p[6] else None,
            category=p[7],
            managed="true"
        )

    AnyNode(
        name="last_update",
        type="datetime",
        value=str(datetime.datetime.now().isoformat()),
        parent=rooms
    )

    logger.info("Generating JSON content for rooms")
    exporter = JsonExporter(indent=2, sort_keys=True)
    data = exporter.export(rooms)

    logger.info("Updating Gitlab repository for rooms")
    user = {"email": "webcast@cern.ch", "first_name": "Automated", "last_name": "Infoream Fetch"}
    gitlab = GitlabClient('92244', 'room_infoream.json', BRANCH, gitlab_access_token=GITLAB_ACCESS_TOKEN)
    result = gitlab.set_file(data, message="Update infoream rooms", user=user)

    logger.info(result)


def generate_codecs_file(codecs_query_results):
    def format_room_name_codec_infoream(room_name):

        if room_name.startswith('CCAC-'):
            room_start = 'CCAC-'
        elif room_name.startswith('CCAB-'):
            room_start = 'CCAB-'
        else:
            room_start = 'CCAD-'

        return \
            room_name.replace(room_start, '').split('-', 1)[0].replace('.', '/', 1).replace('.', '-', 1).split('.', 1)[
                0]

    root = AnyNode(name="codecs", type="list")

    for p in codecs_query_results:
        room_name = "0.0.0"
        if p[7]:
            room_name = p[7]

        room_name = format_room_name_codec_infoream(room_name)

        AnyNode(
            name=p[1],
            type="codec",
            room=room_name,
            parent=root,
            description=p[0],
            status=p[2],
            model=p[4],
            manufacturer=p[6],
            commissioned=p[3].strftime("%Y-%m-%d") if p[3] else None,
            position=p[7],
            value=p[5],
            source="infoream"
        )

    AnyNode(
        name="last_update",
        type="datetime",
        value=str(datetime.datetime.now().isoformat()),
        parent=root
    )

    logger.info("Generate json file for codecs")
    exporter = JsonExporter(indent=2, sort_keys=True)
    data = exporter.export(root)

    logger.info("Updating Gitlab repository for codecs")
    user = {"email": "webcast@cern.ch", "first_name": "Automated", "last_name": "Infoream Fetch"}
    gitlab = GitlabClient('92244', 'codec_infoream.json', BRANCH, gitlab_access_token=GITLAB_ACCESS_TOKEN)
    result = gitlab.set_file(data, message="Update infoream codecs", user=user)

    logger.info(result)


def generate_projectors_file(projectors_query_results):

    def format_room_name_projector_infoream(room_name):
        if room_name.startswith('CCAC-'):
            room_start = 'CCAC-'
        elif room_name.startswith('CCAB-'):
            room_start = 'CCAB-'
        elif room_name.startswith('CCAV-'):
            room_start = 'CCAV-'
        else:
            room_start = 'CCAP-'

        return room_name.replace(room_start, '').split('-', 1)[0].replace('.', '/', 1).replace('.', '-', 1)

    root = AnyNode(name="projectors", type="list")

    for p in projectors_query_results:
        room_name = format_room_name_projector_infoream(p[7])

        AnyNode(
            name=p[1],
            type="projector",
            room=room_name,
            parent=root,
            description=p[0],
            status=p[2],
            model=p[4],
            manufacturer=p[6],
            commissioned=p[3].strftime("%Y-%m-%d") if p[3] else None,
            position=p[7],
            value=p[5],
            source="infoream"
        )

    AnyNode(
        name="last_update",
        type="datetime",
        value=str(datetime.datetime.now().isoformat()),
        parent=root
    )

    logger.info("Generate json file for projectors")
    exporter = JsonExporter(indent=2, sort_keys=True)
    data = exporter.export(root)

    logger.info("Updating Gitlab repository for projectors")
    user = {"email": "webcast@cern.ch", "first_name": "Automated", "last_name": "Infoream Fetch"}
    gitlab = GitlabClient('92244', 'projector_infoream.json', BRANCH, gitlab_access_token=GITLAB_ACCESS_TOKEN)
    result = gitlab.set_file(data, message="Update infoream projectors", user=user)
    logger.info(result)

def generate_pcs_file(pcs_query_results):
    def format_room_name_pc_infoream(room_name):

        if room_name.startswith("PPC-"):
            room_start = 'PPC-'
        else:
            room_start = 'CWD-'

        return \
            room_name.replace(room_start, '').split('-', 1)[0].replace('.', '/', 1).replace('.', '-', 1).split('.', 1)[
                0]

    root = AnyNode(name="pcs", type="list")

    for p in pcs_query_results:
        room_name = format_room_name_pc_infoream(p[7] if p[7] else "0.0.0")

        AnyNode(
            name=p[1],
            type="pc",
            room=room_name,
            parent=root,
            code=p[0],
            status=p[2],
            model=p[4],
            manufacturer=p[6],
            commissioned=p[3].strftime("%Y-%m-%d") if p[3] else None,
            position=p[7],
            value=p[5],
            description=p[8],
            source="infoream"
        )

    AnyNode(
        name="last_update",
        type="datetime",
        value=str(datetime.datetime.now().isoformat()),
        parent=root
    )

    logger.info("Generate json file for pcs")
    exporter = JsonExporter(indent=2, sort_keys=True)
    data = exporter.export(root)

    logger.info("Updating Gitlab repository for pcs")
    user = {"email": "webcast@cern.ch", "first_name": "Automated", "last_name": "Infoream Fetch"}
    gitlab = GitlabClient('92244', 'pc_infoream.json', BRANCH, gitlab_access_token=GITLAB_ACCESS_TOKEN)
    result = gitlab.set_file(data, message="Update infoream pcs", user=user)
    logger.info(result)


def generate_video_systems_file(pcs_query_results):
    def format_room_name_video_system_infoream(room_name):

        if room_name.startswith("PPC-"):
            room_start = 'PPC-'
        else:
            room_start = 'CWD-'

        return \
            room_name.replace(room_start, '').split('-', 1)[0].replace('.', '/', 1).replace('.', '-', 1).split('.', 1)[
                0]

    root = AnyNode(name="video_system", type="list")

    for p in pcs_query_results:
        room_name = format_room_name_video_system_infoream(p[7] if p[7] else "0.0.0")

        AnyNode(
            name=p[1],
            type="video_system",
            room=room_name,
            parent=root,
            code=p[0],
            status=p[2],
            model=p[4],
            manufacturer=p[6],
            commissioned=p[3].strftime("%Y-%m-%d") if p[3] else None,
            position=p[7],
            value=p[5],
            description=p[8],
            source="infoream"
        )

    AnyNode(
        name="last_update",
        type="datetime",
        value=str(datetime.datetime.now().isoformat()),
        parent=root
    )

    logger.info("Generate json file for pcs")
    exporter = JsonExporter(indent=2, sort_keys=True)
    data = exporter.export(root)

    logger.info("Updating Gitlab repository for video_systems")
    user = {"email": "webcast@cern.ch", "first_name": "Automated", "last_name": "Infoream Fetch"}
    gitlab = GitlabClient('92244', 'video_system_infoream.json', BRANCH, gitlab_access_token=GITLAB_ACCESS_TOKEN)
    result = gitlab.set_file(data, message="Update infoream video_systems", user=user)
    logger.info(result)


def generate_parts_file(loans_query_results):

    def format_room_name_part_infoream(room_name):

        if room_name.startswith("CCA-"):
            room_start = 'CCA-'
        else:
            room_start = 'AV-'

        new_name = \
            room_name.replace(room_start, '').split('-', 1)[0].replace('.', '/', 1).replace('.', '-', 1).replace('#',
                                                                                                                 '',
                                                                                                                 1).replace(
                '*', '', 1).split('.', 1)[0]

        return new_name

    root = AnyNode(name="loans", type="list")

    for p in loans_query_results:
        room_name = format_room_name_part_infoream(p[2] if p[2] else "0.0.0")

        AnyNode(
            name=p[1],
            description=p[1],
            type="part",
            room=room_name,
            parent=root,
            code=p[0],
            epa_code=p[2],
            quantity=p[3],
            base_price=p[4],
            source="infoream"
        )

    AnyNode(
        name="last_update",
        type="datetime",
        value=str(datetime.datetime.now().isoformat()),
        parent=root
    )

    logger.debug("Generate json file for parts")
    exporter = JsonExporter(indent=2, sort_keys=True)
    data = exporter.export(root)

    logger.info("Updating Gitlab repository for parts")
    user = {"email": "webcast@cern.ch", "first_name": "Automated", "last_name": "Infoream Fetch"}
    gitlab = GitlabClient('92244', 'part_infoream.json', BRANCH, gitlab_access_token=GITLAB_ACCESS_TOKEN)
    result = gitlab.set_file(data, message="Update infoream parts", user=user)
    logger.info(result)


def generate_loans_file(loans_query_results):
    root = AnyNode(name="loans", type="list")

    for p in loans_query_results:
        category_name = p[2]

        AnyNode(
            name=p[1],
            description=p[1],
            type="loan",
            category=category_name,
            parent=root,
            code=p[0],
            serial_number=p[3],
            model=p[4],
            manufacturer=p[5],
            status=p[6],
            source="infoream"
        )

    AnyNode(
        name="last_update",
        type="datetime",
        value=str(datetime.datetime.now().isoformat()),
        parent=root
    )

    logger.debug("Generate json file for loans")
    exporter = JsonExporter(indent=2, sort_keys=True)
    data = exporter.export(root)

    logger.info("Updating Gitlab repository for loans")
    user = {"email": "webcast@cern.ch", "first_name": "Automated", "last_name": "Infoream Fetch"}
    gitlab = GitlabClient('92244', 'loan_infoream.json', BRANCH, gitlab_access_token=GITLAB_ACCESS_TOKEN)
    result = gitlab.set_file(data, message="Update infoream loans", user=user)
    logger.info(result)


def generate_controllers_file(controllers_query_results):

    def format_room_name_controller_infoream(room_name):

        if room_name.startswith("CCAC-"):
            room_start = 'CCAC-'
        else:
            room_start = 'CCAVS-'

        new_name = \
            room_name.replace(room_start, '').split('-', 1)[0].replace('.', '/', 1).replace('.', '-', 1).replace('#',
                                                                                                                 '',
                                                                                                                 1).replace(
                '*', '', 1).split('.', 1)[0]

        return new_name

    root = AnyNode(name="controllers", type="list")

    for p in controllers_query_results:
        room_name = format_room_name_controller_infoream(p[7] if p[7] else "0.0.0")

        AnyNode(
            name=p[1],
            description=p[1],
            type="controller",
            room=room_name,
            parent=root,
            code=p[0],
            status=p[2],
            model=p[4],
            manufacturer=p[6],
            value=p[5],
            commissioned=p[3].strftime("%Y-%m-%d") if p[3] else None,
            position=p[7],
            source="infoream"
        )

    AnyNode(
        name="last_update",
        type="datetime",
        value=str(datetime.datetime.now().isoformat()),
        parent=root
    )

    logger.debug("Generate json file for parts")
    exporter = JsonExporter(indent=2, sort_keys=True)
    data = exporter.export(root)

    logger.info("Updating Gitlab repository for parts")
    user = {"email": "webcast@cern.ch", "first_name": "Automated", "last_name": "Infoream Fetch"}
    gitlab = GitlabClient('92244', 'controller_infoream.json', BRANCH, gitlab_access_token=GITLAB_ACCESS_TOKEN)
    result = gitlab.set_file(data, message="Update infoream parts", user=user)
    logger.info(result)


def fetch_infoream_rooms():
    logger.debug('Updating rooms from Infoream. RUN_MODE: {}'.format(os.environ.get("RUN_MODE", "UNDEFINED")))
    rooms = InforeamConnector().get_rooms_managed_by_service()
    generate_rooms_file(rooms)


def fetch_infoream_codecs():
    logger.debug('Updating codecs from Infoream. RUN_MODE: {}'.format(os.environ.get("RUN_MODE", "UNDEFINED")))
    codecs = InforeamConnector().get_codecs()
    generate_codecs_file(codecs)


def fetch_infoream_projectors():
    logger.debug(
        'Updating projectors from Infoream. RUN_MODE: {}'.format(os.environ.get("RUN_MODE", "UNDEFINED")))
    projectors = InforeamConnector().get_projectors()
    generate_projectors_file(projectors)


def fetch_infoream_controllers():
    logger.debug('Updating controllers from Infoream. RUN_MODE: {}'.format(os.environ.get("RUN_MODE",
                                                                                          "UNDEFINED")))
    controllers = InforeamConnector().get_controllers()
    generate_controllers_file(controllers)


def fetch_infoream_video_systems():
    logger.debug('Updating video_systems from Infoream. RUN_MODE: {}'.format(os.environ.get("RUN_MODE",
                                                                                          "UNDEFINED")))
    video_system = InforeamConnector().get_video_systems()
    generate_video_systems_file(video_system)


def fetch_infoream_parts():
    logger.debug('Updating parts from Infoream. RUN_MODE: {}'.format(os.environ.get("RUN_MODE", "UNDEFINED")))
    parts = InforeamConnector().get_parts()
    generate_parts_file(parts)


def fetch_infoream_loans():
    logger.debug('Updating loans from Infoream. RUN_MODE: {}'.format(os.environ.get("RUN_MODE", "UNDEFINED")))
    loans = InforeamConnector().get_loans()
    generate_loans_file(loans)


def fetch_infoream_pcs():
    logger.debug('Updating pcs from Infoream. RUN_MODE: {}'.format(os.environ.get("RUN_MODE", "UNDEFINED")))
    pcs = InforeamConnector().get_pcs()
    generate_pcs_file(pcs)