import click
from flask.cli import with_appcontext
import logging
from datetime import datetime
import os
from apscheduler.schedulers.blocking import BlockingScheduler

from app.extensions import db
from app.services.infoream_service import InforeamConnector
from app.utils.infoream_utils import generate_rooms_file, generate_codecs_file, generate_projectors_file, \
    generate_pcs_file, generate_parts_file, generate_loans_file, generate_controllers_file, fetch_infoream_rooms, \
    fetch_infoream_codecs, fetch_infoream_projectors, fetch_infoream_controllers, fetch_infoream_parts, \
    fetch_infoream_pcs, fetch_infoream_loans, generate_video_systems_file, fetch_infoream_video_systems


@click.command()
@with_appcontext
def echo_command():
    """
    Prints a message
    """
    click.echo('Testing is ok')
    return 0

@click.command()
@with_appcontext
def db_init():
    """
    Initialize the database.
    """
    click.echo('Initializing the db')
    db.create_all()


@click.command()
@with_appcontext
def test_job():
    """
    Runs a job that prints a message.
    Run it like: "RUN_MODE=JOB flask log-job"
    """
    logger = logging.getLogger("job.log_job")

    def tick():
        logger.info(
            'Tick! The time is: {} RUN_MODE: {}'.format(datetime.now(), os.environ.get("RUN_MODE", "UNDEFINED")))

    scheduler = BlockingScheduler()
    scheduler.add_job(tick, 'interval', seconds=60)
    logger.debug('Press Ctrl+c to exit')

    try:
        scheduler.start()
    except (KeyboardInterrupt, SystemExit):
        logger.info("Daemon stopped")


@click.command()
@with_appcontext
def get_info_rooms():
    """
    Get the rooms from infoream
    """
    rooms = InforeamConnector().get_rooms_managed_by_service()
    generate_rooms_file(rooms)

    return 0


@click.command()
@with_appcontext
def get_info_codecs():
    """
    Get the codecs from infoream
    """
    codecs = InforeamConnector().get_codecs()
    generate_codecs_file(codecs)

    return 0


@click.command()
@with_appcontext
def get_info_projectors():
    """
    Get the projectors from infoream
    """
    projectors = InforeamConnector().get_projectors()
    generate_projectors_file(projectors)

    return 0


@click.command()
@with_appcontext
def get_info_pcs():
    """
    Get the pcs from infoream
    """
    pcs = InforeamConnector().get_pcs()
    generate_pcs_file(pcs)

    return 0


@click.command()
@with_appcontext
def get_info_video_systems():
    """
    Get the encoders from infoream
    """
    pcs = InforeamConnector().get_video_systems()
    generate_video_systems_file(pcs)

    return 0

@click.command()
@with_appcontext
def get_info_parts():
    """
    Get the parts from infoream
    """
    parts = InforeamConnector().get_parts()
    generate_parts_file(parts)

    return 0


@click.command()
@with_appcontext
def get_info_loans():
    """
    Get the loans from infoream
    """
    loans = InforeamConnector().get_loans()
    generate_loans_file(loans)

    return 0


@click.command()
@with_appcontext
def get_info_controllers():
    """
    Get the loans from infoream
    """
    controllers = InforeamConnector().get_controllers()
    generate_controllers_file(controllers)

    return 0


def load_infoream_commands(app):
    logger = logging.getLogger("job.info_rooms_job")

    def get_rooms_managed_by_service():
        with app.app_context():
            fetch_infoream_rooms()

    def get_codecs_managed_by_service():
        with app.app_context():
            fetch_infoream_codecs()

    def get_projectors_managed_by_service():
        with app.app_context():
            fetch_infoream_projectors()

    def get_pcs_managed_by_service():
        with app.app_context():
            fetch_infoream_pcs()

    def get_video_systems_managed_by_service():
        with app.app_context():
            fetch_infoream_video_systems()

    def get_parts_managed_by_service():
        with app.app_context():
            fetch_infoream_parts()

    def get_loans_managed_by_service():
        with app.app_context():
            fetch_infoream_loans()

    def get_controllers_managed_by_service():
        with app.app_context():
            fetch_infoream_controllers()



    @app.cli.command("infoream-job")
    def infoream_job():
        """
        Fetches all the managed elements from Infoream
        :return:
        """
        scheduler = BlockingScheduler()
        scheduler.add_job(get_rooms_managed_by_service, 'cron', hour=7, minute=10)
        scheduler.add_job(get_codecs_managed_by_service, 'cron', hour=7, minute=12)
        scheduler.add_job(get_projectors_managed_by_service, 'cron', hour=7, minute=14)
        scheduler.add_job(get_pcs_managed_by_service, 'cron', hour=7, minute=16)
        scheduler.add_job(get_parts_managed_by_service, 'cron', hour=7, minute=18)
        scheduler.add_job(get_loans_managed_by_service, 'cron', hour=7, minute=20)
        scheduler.add_job(get_controllers_managed_by_service, 'cron', hour=7, minute=22)
        scheduler.add_job(get_video_systems_managed_by_service, 'cron', hour=7, minute=24)
        logger.info('Press Ctrl+c to exit')

        try:
            scheduler.start()
        except (KeyboardInterrupt, SystemExit):
            logger.warning("info-rooms-job JOB terminated")


def initialize_cli(app):
    """
    Add additional commands to the CLI. These are loaded automatically on the main.py

    :param app: App to attach the cli commands to
    :return: None
    """
    app.cli.add_command(echo_command)
    app.cli.add_command(db_init)
    app.cli.add_command(test_job)
    # Rooms
    app.cli.add_command(get_info_rooms)
    load_infoream_commands(app)
    # Codecs
    app.cli.add_command(get_info_codecs)
    # Projectors
    app.cli.add_command(get_info_projectors)
    # PCs
    app.cli.add_command(get_info_pcs)
    # Parts
    app.cli.add_command(get_info_parts)
    # Loans
    app.cli.add_command(get_info_loans)
    # Controllers
    app.cli.add_command(get_info_controllers)
    # Encoders
    app.cli.add_command(get_info_video_systems)