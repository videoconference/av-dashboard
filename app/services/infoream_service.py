from flask import current_app

from app.services.oracle_service import OracleDbManager


class InforeamConnector(object):

    def __init__(self):
        self._configuration = current_app.config.get('ORACLE_DB_CONFIG')

        self._configuration = {
            'ORACLE_USER': current_app.config.get('ORACLE_USER', None),
            'ORACLE_PASSWORD': current_app.config.get('ORACLE_PASSWORD', None),
            'ORACLE_HOST': current_app.config.get('ORACLE_HOST', None),
            'ORACLE_PORT': current_app.config.get('ORACLE_PORT', None),
            'ORACLE_SERVICE': current_app.config.get('ORACLE_SERVICE', None),
        }

        self._oracle_db_manager = OracleDbManager(self._configuration)

    def get_rooms_managed_by_service(self):
        """
        Get all the rooms managed by the service
        """
        """
        
        """
        query_string = """
        SELECT obj_code, obj_desc, obj_status, obj_commiss,r5p1.prv_value sponsor,r5p2.prv_value comments,r5p3.prv_dvalue expecteddate, obj_category 
        FROM r5objects 
        LEFT JOIN r5propertyvalues r5p1 on r5p1.prv_property = 'AV008' and r5p1.prv_code=obj_code || '#*' 
        LEFT JOIN r5propertyvalues r5p2 on r5p2.prv_property = 'AV022' and r5p2.prv_code=obj_code || '#*' 
        LEFT JOIN r5propertyvalues r5p3 on r5p3.prv_property = 'AV023' and r5p3.prv_code=obj_code || '#*' 
        WHERE obj_class='AVR' AND obj_code LIKE 'CCA-%' AND obj_status IN ('I', 'IE', 'IRP', 'DP', 'MD', 'ID') AND obj_obtype='P'
        """
        result = self._oracle_db_manager.execute(query_string)
        return result

    def get_codecs(self):
        """
        Get all the codecs
        """
        query_string = """
        SELECT obj_code, obj_desc, obj_status, obj_commiss, obj_manufactmodel, obj_value, mfg_desc, obj_position
        FROM r5objects
        LEFT JOIN r5manufacturers on obj_manufact=mfg_code
        WHERE obj_class='AVV' and obj_obtype='A' and obj_status='I'
        """
        result = self._oracle_db_manager.execute(query_string)
        return result

    def get_video_systems(self):
        query_string = """
        SELECT obj_code, obj_desc, obj_status, obj_commiss, obj_manufactmodel, obj_value, mfg_desc, obj_position, cat_desc
        FROM r5objects
        LEFT JOIN r5categories on cat_code=obj_category
        LEFT JOIN r5manufacturers on obj_manufact=mfg_code
        WHERE obj_class='AVVS' and obj_obtype='A' and obj_status='I'
        """
        result = self._oracle_db_manager.execute(query_string)
        return result

    def get_projectors(self):
        """
        Get all projectors
        """
        query_string = """
        SELECT obj_code, obj_desc, obj_status, obj_commiss, obj_manufactmodel, obj_value, mfg_desc, obj_position
        FROM r5objects
        LEFT JOIN r5manufacturers on obj_manufact=mfg_code
        WHERE obj_class='AVD' and obj_category='AV-PRO' and obj_obrtype='A' and obj_position!='null' and obj_status='I'
        """
        result = self._oracle_db_manager.execute(query_string)
        return result

    def get_av_equipment(self):
        fields = "obj_code, obj_desc, obj_status, obj_commiss, obj_manufactmodel, obj_value, mfg_desc, obj_position, " \
                 "cls_desc, cat_desc"
        query_string = "SELECT " + fields + " " \
                                            "FROM r5objects " \
                                            "LEFT JOIN r5categories on cat_code=obj_category " \
                                            "LEFT JOIN r5manufacturers on obj_manufact=mfg_code " \
                                            "LEFT JOIN r5classes on cls_code=obj_class and cls_rentity='OBJ' " \
                                            "WHERE obj_obtype='A' and obj_status='I' and obj_mrc='AV'"
        result = self._oracle_db_manager.execute(query_string)
        return result

    def get_pcs(self):
        query_string = """
        SELECT obj_code, obj_desc, obj_status, obj_commiss, obj_manufactmodel, obj_value, mfg_desc, obj_position, cat_desc
        FROM r5objects
        LEFT JOIN r5categories on cat_code=obj_category
        LEFT JOIN r5manufacturers on obj_manufact=mfg_code
        WHERE obj_class='AVC' and obj_obtype='A' and obj_status='I'
        """
        result = self._oracle_db_manager.execute(query_string)
        return result

    def get_parts(self):
        query_string = """
        SELECT epa_part, par_desc, epa_code, epa_qty, par_baseprice
        FROM r5entityparts
        LEFT JOIN r5parts ON epa_part=par_code
        WHERE epa_part LIKE 'AV-%' AND epa_code like 'CCA-%'
        """
        result = self._oracle_db_manager.execute(query_string)
        return result

    def get_loans(self):
        query_string = """
        SELECT obj_code,obj_desc,par_desc,obj_serialno,obj_manufactmodel,mfg_desc,obj_status
        FROM r5objects
        LEFT JOIN r5parts on obj_part=par_code
        LEFT JOIN r5manufacturers on obj_manufact=mfg_code
        WHERE obj_store='AV04'
        ORDER BY obj_part
        """
        result = self._oracle_db_manager.execute(query_string)
        return result

    def get_controllers(self):
        query_string = """
        SELECT obj_code, obj_desc, obj_status, obj_commiss, obj_manufactmodel, obj_value, mfg_desc, obj_position
        FROM r5objects
        LEFT JOIN r5manufacturers on obj_manufact=mfg_code
        WHERE obj_class='AVS' and obj_obrtype='A' and obj_position!='null' and obj_status='I'
        """
        result = self._oracle_db_manager.execute(query_string)
        return result

    def get_av_equipment_by_room(self, room_name):

        # Process the room_name
        if room_name == "":
            return False

        room_name = room_name.replace('-', '.')

        fields = "obj_code, obj_desc, obj_status, obj_commiss, obj_manufactmodel, obj_value, mfg_desc, obj_position, " \
                 "cls_desc, cat_desc"
        query_string = "SELECT " + fields + " " \
                        "FROM r5objects " \
                        "LEFT JOIN r5categories on cat_code=obj_category " \
                        "LEFT JOIN r5manufacturers on obj_manufact=mfg_code " \
                        "LEFT JOIN r5classes on cls_code=obj_class and cls_rentity='OBJ' " \
                        "WHERE obj_obtype='A' and obj_status='I' and obj_mrc='AV' " \
                                            "and obj_position like '%" + room_name.strip() + "%' " \
                                            "and cls_desc='video conference'"
        result = self._oracle_db_manager.execute(query_string)

        return result

    def get_webcast_equipment_by_room(self, room_name):

        # Process the room_name
        if room_name == "":
            return False
        # print "get_webcasts..."
        room_name = room_name.replace('-', '.')

        fields = "obj_code, obj_desc, obj_status, obj_commiss, obj_manufactmodel, obj_value, mfg_desc, obj_position, " \
                 "cls_desc, cat_desc"
        query_string = "SELECT " + fields + " " \
                        "FROM r5objects " \
                        "LEFT JOIN r5categories on cat_code=obj_category " \
                        "LEFT JOIN r5manufacturers on obj_manufact=mfg_code " \
                        "LEFT JOIN r5classes on cls_code=obj_class and cls_rentity='OBJ' " \
                        "WHERE obj_position like '%" + room_name.strip() + "%' " \
                               "and cat_desc='Encoding PC'"
        result = self._oracle_db_manager.execute(query_string)
        # print query_string
        # print result

        return result

    def get_pc_equipment_by_room(self, room_name):

        # Process the room_name
        if room_name == "":
            return False
        room_name = room_name.replace('-', '.')

        fields = "obj_code, obj_desc, obj_status, obj_commiss, obj_manufactmodel, obj_value, mfg_desc, obj_position, " \
                 "cls_desc, cat_desc"
        query_string = "SELECT " + fields + " " \
                        "FROM r5objects " \
                        "LEFT JOIN r5categories on cat_code=obj_category " \
                        "LEFT JOIN r5manufacturers on obj_manufact=mfg_code " \
                        "LEFT JOIN r5classes on cls_code=obj_class and cls_rentity='OBJ' " \
                        "WHERE obj_position like '%" + room_name.strip() + "%' " \
                               "and cat_desc='Presentation PC'"
        result = self._oracle_db_manager.execute(query_string)

        return result

    def get_number_of_meeting_rooms(self):
        query_string = """
        SELECT
        COUNT(LOC_CODE)
        FROM r5locations WHERE
        LOC_DESC in ('SALLE DE REUNION', 'SALLE DE CONFERENCE', 'SALLE DE COURS')
        """
        result = self._oracle_db_manager.execute(query_string)

        return result[0][0]

    def get_number_of_meeting_rooms_managed(self):
        query_string = """
        SELECT count(obj_code) 
        FROM r5objects 
        LEFT JOIN r5propertyvalues r5p1 on r5p1.prv_property = 'AV008' and r5p1.prv_code=obj_code || '#*' 
        LEFT JOIN r5propertyvalues r5p2 on r5p2.prv_property = 'AV022' and r5p2.prv_code=obj_code || '#*' 
        LEFT JOIN r5propertyvalues r5p3 on r5p3.prv_property = 'AV023' and r5p3.prv_code=obj_code || '#*' 
        WHERE obj_class='AVR' AND obj_code LIKE 'CCA-%' AND obj_status IN ('I', 'IE', 'IRP', 'DP', 'MD', 'ID') AND obj_obtype='P'
        """
        result = self._oracle_db_manager.execute(query_string)

        return result[0][0]

    def get_number_of_equipment_with_manufacturer(self):
        query_string = """
        SELECT ROUND((
        SELECT count(*) 
        FROM r5objects where obj_code like 'CCAX-%' and  obj_manufact is not null
              )/(
        SELECT count(*)
        FROM r5objects where obj_code like 'CCAX-%'
            ) * 100) as nb_manufact
        FROM dual
        """
        result = self._oracle_db_manager.execute(query_string)

        return result[0][0]

    def get_number_of_equipment_with_serial_number(self):
        query_string = """
        SELECT ROUND((
        SELECT count(*) 
        FROM r5objects where obj_code like 'CCAX-%' and  obj_serialno is not null
              )/(
        SELECT count(*)
        FROM r5objects where obj_code like 'CCAX-%'
              ) * 100) as nb_serialno
        FROM dual
        """
        result = self._oracle_db_manager.execute(query_string)

        return result[0][0]

    def get_number_of_equipment_inventiored(self):
        query_string = """
        SELECT ROUND(
        (SELECT COUNT(obj_code)
        FROM r5objects
        LEFT JOIN r5propertyvalues on prv_property = 'AV024' and prv_code=obj_code || '#*'
        WHERE prv_dvalue is not null AND obj_class='AVR' AND obj_code     LIKE 'CCA-%' AND obj_status='I'
              )/(
        SELECT COUNT(obj_code)
        FROM r5objects
        WHERE obj_class='AVR' AND obj_code LIKE 'CCA-%' AND obj_status='I'
              ) * 100) as nb
        FROM dual
        """
        result = self._oracle_db_manager.execute(query_string)

        return result[0][0]
