import cx_Oracle
import logging

logger = logging.getLogger('webapp.oracle')


class OracleDbManager(object):

    def __init__(self, configuration=None):

        if configuration is None:
            configuration = dict()

        self._check_init_preconditions(configuration)
        self._configuration = configuration

    def _check_init_preconditions(self, configuration):

        user = configuration.get('ORACLE_USER', None)
        password = configuration.get('ORACLE_PASSWORD', None)
        host = configuration.get('ORACLE_HOST', None)
        port = configuration.get('ORACLE_PORT', None)
        service = configuration.get('ORACLE_SERVICE', None)

        if user is None or user == '':
            raise ValueError('User must be set')
        if password is None or password == '':
            raise ValueError('Password must be set')
        if host is None or host == '':
            raise ValueError('Host must be set')
        if port is None or port == '':
            raise ValueError('Port must be set')
        if service is None or service == '':
            raise ValueError('Service must be set')

    def _connect(self):
        """
            Create a new connection with the Oracle database
        """
        database = None
        try:
            # connect to the database
            database = cx_Oracle.connect(self._configuration.get('ORACLE_USER', None) +
                                         '/' +
                                         self._configuration.get('ORACLE_PASSWORD', None) +
                                         '@' +
                                         self._configuration.get('ORACLE_HOST', None) +
                                         ':' +
                                         self._configuration.get('ORACLE_PORT', None) +
                                         '/' +
                                         self._configuration.get('ORACLE_SERVICE', None))
        except cx_Oracle.Error as err:
            logger.exception("Error while connecting to Oracle database: %s" % err)
            return None

        return database

    def execute(self, query_string, params=None, commit=False, fetch_all=True):

        if params is None:
            params = dict()

        rows = []
        database = None

        # if params is list, convert them from list to tuple
        if isinstance(params, list):
            params = tuple(params)
        cursor = None
        try:
            print("Connecting database")
            database = self._connect()
            try:
                cursor = database.cursor()
                cursor.execute(query_string, params)

                # fetch results
                if fetch_all:
                    rows = cursor.fetchall()
                else:
                    rows = cursor.fetchone()

                if commit:
                    database.commit()
            except AttributeError as e:
                print("Error while connecting to Oracle database: %s" % e)
            finally:
                if cursor:
                    cursor.close()

        except cx_Oracle.Error as err:
            print("Error while connecting to Oracle database: %s" % err)

        finally:
            if database:
                database.close()

        return rows
