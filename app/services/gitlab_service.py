import json
import logging
import requests
from flask import current_app, g
from app.extensions import cache

logger = logging.getLogger("webapp.gitlab_service")


class GitlabClientException(Exception):
    pass


def build_request_data(data, branch, user=None, message="Update the data file"):
    """
    Build the data for the Gitlab API request

    @param data: Content of the file in JSON format
    @type data: JSON
    @param branch: Branch of the commit (master, qa, etc)
    @type branch: string
    @param user: The user that will appear on the Gitlab commit
    @type user: Object
    @param message: Message of the commit
    @type message: string
    @return: dict with the format expected by Gitlab API
    """
    if not user:
        user = g.user

    try:
        data_to_send = {
            "branch": branch,
            "author_email": user["email"],
            "author_name": user["first_name"] + " " + user["last_name"],
            "content": data,
            "commit_message": message
        }
    except KeyError as e:
        raise GitlabClientException("User must contain the required fields: (ERROR: {})".format(e))

    return data_to_send


class GitlabClient:

    def __init__(self, project_id, file_path, branch, gitlab_access_token=None):
        """
        Constructor

        @param project_id: The project ID on Gitlab
        @type project_id: string
        @param file_path: Path of the file to modify and retrieve in the Gitlab repository
        @type file_path: string
        @param branch: Branch in the Gitlab repository
        @type branch: string
        @param gitlab_access_token: Access token of the user with access to the repository.
                                    The recommended thing is to have a user with access ONLY
                                    to the repository that we are manipulating.
        @type gitlab_access_token: string
        """
        if not gitlab_access_token:
            gitlab_access_token = current_app.config.get('GITLAB_ACCESS_TOKEN', None)

        self.headers = {
            'PRIVATE-TOKEN': gitlab_access_token,
            "Content-Type": "application/json"
        }
        self.repo_url = 'https://gitlab.cern.ch'
        self.api_get_path = '/api/v4/projects/{}/repository/files/{}?ref={}'.format(project_id, file_path, branch)
        self.api_set_path = '/api/v4/projects/{}/repository/files/{}'.format(project_id, file_path)
        self.branch = branch

    def get_file(self):
        """
        Gets a file from Gitlab repository.
        The file obtained is the one set when the GitlabClient is created
        """
        url = self.repo_url + self.api_get_path
        result = requests.get(url, headers=self.headers)
        if result.status_code != 200:
            logger.error("Error {}. Credentials invalid: {}".format(result.status_code, result.text))
            return False
        result.raise_for_status()

        return result.json()

    def set_file(self, data, message=None, user=None):
        """
        Sets a file in the Gitlab repository

        @param data: Content of the file that will be pushed
        @type data: dict
        @param message: Message of the commit
        @type message: string
        @param user: User that will appear in the commit
        @type user: Object
        @return:
        """
        url = self.repo_url + self.api_set_path

        data = build_request_data(data, self.branch, message=message, user=user)
        result = requests.put(url, data=json.dumps(data), headers=self.headers)

        if result.status_code != 200:
            logger.error("Error {}. Credentials invalid: {}".format(result.status_code, result.text))
            return False

        result.raise_for_status()

        return result.json()
