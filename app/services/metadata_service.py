import base64
import datetime
import json
import logging
from json.decoder import JSONDecodeError

from anytree import findall, AnyNode, find
from anytree.exporter import JsonExporter, DictExporter
from anytree.importer import DictImporter
from flask import g, current_app
from requests import HTTPError

from app.extensions import cache
from app.services.gitlab_service import GitlabClient
from secret.config import GITLAB_ACCESS_TOKEN

logger = logging.getLogger("webapp.metadata_client")


class MetadataClientError(Exception):
    pass


def get_metadata_client(resource_name):
    logger.info("Loading not cached Metadata Client")
    client = MetadataClient(resource_name, branch=current_app.config["BRANCH"])
    return client


class MetadataClient:
    allowed_resources = ['codec', 'controller', 'video_system', 'loan', 'part', 'pc', 'projector', 'room']

    def __init__(self, resource_name, allow_infoream_edit=False, branch=None):
        """
        MetadataClient class, that handles the metadata store by the application.

        @param resource_name: Resource type to be fetched from Gitlab. This must be one
                              of the supported resources: codec, loan, part, controller, projector,
                              etc. It should match the name of the file in the repository:
                              Eg: codec_infoream.json or codec_custom.json.
        @type resource_name: string
        @param allow_infoream_edit: Whether or not the client instance should allow to edit infoream
                                    data
        @type allow_infoream_edit: bool
        @param branch: The branch that will be modified in the Gitlab repository:
                       Eg: master, qa, test, etc.
        @type branch: string
        """
        if branch is None:
            raise MetadataClientError("Branch is mandatory")

        logger.info("Initializing MetadataClient with branch {}".format(branch))
        # Set the parameters of the instance
        self.files = [resource_name + '_' + 'infoream.json', resource_name + '_custom.json']
        self.metadata = {}
        self.allow_infoream_edit = allow_infoream_edit
        self.branch = branch

        # We initialize the client with the metadata from Gitlab
        self.get_metadata_from_gitlab()

    def get_metadata_from_gitlab(self):
        """
        Get the latest version of the metadata in json format from the Gitlab API
        All the self.files will be retrieved from the repository.
        The data is set on the metadata attribute of the class
        The format of the files will be the following (For codec resource type):
        ```json
        {'codec_custom': {'hello': 'world'}, 'codec_infoream': {'hello': 'world'}}
        ```
        """
        logger.info("Obtaining metadata from GitlabClient")
        result = False
        for file in self.files:
            logger.debug("Getting metadata for file: {}".format(file))
            client = GitlabClient('92244', file, self.branch)
            try:
                data = client.get_file()
                if not data:
                    break
                    # raise MetadataClientError("Unable to retrieve data from the repository")
                logger.debug("Obtained metadata from file: {}".format(file))
                file = file.split(".")[0]
                self.metadata[file] = data
                result = True
            except (JSONDecodeError, HTTPError) as e:
                logger.warning("File {} not found. Not setting it on metadata (ERROR: {})".format(file, e))
                result = False
        return result

    def get_current_commit_id(self, resource_type):
        """
        Get the current commit id stored locally (cached) in the client
        This last_commit_id was obtained when the data was fetched from Gitlab and stored on
        the cache. The path in the metadata is the following:
        - metadata
        -- codec_custom
        --- last_commit_id
        @param resource_type: The type of the resource we are getting: codec, projector, etc.
        @type resource_type: string
        @return: string with the last_commit_id
        """
        if not self.metadata[resource_type + "_custom"]:
            error_message = """
                Unable to get the local commit ID for {}. Does the '{}_custom.json' file exists?
                """.format(resource_type, resource_type)
            logger.error(error_message)
            raise MetadataClientError(error_message)
        return self.metadata[resource_type + "_custom"]["last_commit_id"]

    def get_remote_commit_id(self, resource_type):
        """
        Get the current commit id from the branch of the Gitlab repository
        @param resource_type:
        @return:
        """
        client = GitlabClient('92244', resource_type + "_custom.json", self.branch)
        data = client.get_file()
        commit_id = data["last_commit_id"]

        return commit_id

    def set_metadata_to_gitlab(self, data, resource_type, branch):
        """
        Set the local metadata (cached in the application) to the Gitlab repository

        @param data: The data that will be sent to the Gitlab repository (file content)
        @type data: dict
        @param resource_type: The name of the resource: codec, projector, controller, etc
        @type resource_type: string
        @param branch: Name of the Gitlab branch we are working on: master, qa, test, etc
        @type branch: string
        @return:
        """
        # Get the current version of the metadata from Gitlab API
        # Compare the commit ID
        current_commit_id = self.get_current_commit_id(resource_type)
        remote_commit_id = self.get_remote_commit_id(resource_type)
        result = False
        if current_commit_id == remote_commit_id:
            user = g.user
            gitlab = GitlabClient('92244', resource_type + '_custom.json', branch,
                                  gitlab_access_token=GITLAB_ACCESS_TOKEN)
            result = gitlab.set_file(data, message="Update custom resource", user=user)
        else:
            # If it is not the same, don't allow to update and ask to fetch again
            logger.error(
                "The commit id does not match remote {} vs {}. Please update local metadata before updating.".format(
                    current_commit_id, remote_commit_id))

        return result

    def set_resource_by_type_json(self, parent_type=None, parent_name=None, resource_type=None, resource_name=None,
                                  new_data=None):
        logger.debug("Setting up resource")
        disallowed_fields = self.build_disallowed_fields(parent_type)

        result = None
        if resource_type and resource_type not in self.allowed_resources:
            logger.error("Resource type must be one of: {}".format(self.allowed_resources))
            return {'result': 'ERROR'}
        try:
            root = self.get_tree_for_resource(resource_type)

            logger.info(
                "Searching the node name: {} {}:{} type: {}".format(resource_name, parent_type,
                                                                    parent_name, resource_type))
            nodes = findall(root, lambda node: (
                    node.type == resource_type and node.__getattribute__(
                parent_type) == parent_name and node.name == resource_name))
            node_found = False
            if len(nodes) == 0:
                logger.info("No nodes found for {} and type {}".format(resource_name, resource_type))
            else:
                node_found = True

            if node_found:
                logger.info("A node with name:{} already exists. Editing it".format(resource_name))
                logger.info(new_data)

                logger.info(dir(nodes[0]))
                new_node = self.create_new_node(nodes[0].name, nodes[0].type, nodes[0].parent)
                self.set_new_parameters_to_node(new_data, new_node, parent_name, parent_type)
                nodes[0].parent = None

            else:
                logger.info("A node with name:{} doesnt exist. Creating it".format(resource_name))
                new_node = self.create_new_node(resource_name, resource_type, root)
                self.set_new_parameters_to_node(new_data, new_node, parent_name, parent_type)

        except KeyError as e:
            logger.warning("Unable to find key {} in the current metadata. (ERROR: {})".format(resource_type, e))

            # We need to create the key in because it's the first time we do it
            # Create the root
            root = AnyNode(name="root", type="list")
            new_node = self.create_new_node(resource_name, resource_type, root)
            self.set_new_parameters_to_node(new_data, new_node, parent_name, parent_type)

            logger.info("Generate json file for resource")

        if root:
            # Add the last updated node
            self.generate_last_updated_node(root)
            # Export the content of the tree
            # If the version of the metadata doesn't have changes, allow the update
            exporter = JsonExporter(indent=2, sort_keys=True)
            data = exporter.export(root)
            # Save the file in Gitlab
            result = self.set_metadata_to_gitlab(data, resource_type, self.branch)
            # Clear the cache
            logger.info("Invalidating cache for: {}".format(resource_type))
            cache.delete_memoized(get_resource, parent_type, parent_name, resource_type, resource_name)

            # Update the local metadata
            self.get_metadata_from_gitlab()

            if result:
                return {'result': 'OK'}

        return result

    def generate_last_updated_node(self, root):
        last_updated_node = find(root, lambda node: (node.type == 'datetime' and node.name == 'last_update'))
        if not last_updated_node:
            AnyNode(
                name="last_update",
                type="datetime",
                value=str(datetime.datetime.now().isoformat()),
                parent=root
            )
        else:
            last_updated_node.value = str(datetime.datetime.now())

    def get_tree_for_resource(self, resource_type):
        file_content = self.decode_file_content(resource_type)
        try:
            root = self.generate_tree_from_file_content(file_content)
        except JSONDecodeError as e:
            logger.warning(
                """Unable to load the json content from file {}. It seems the file is new.
                 Creating new root... (ERROR: {})""".format(
                    resource_type + "_custom", e))
            root = AnyNode(name="root", type="list")
        return root

    def decode_file_content(self, resource_type):
        logger.info("Loading json from content")
        file_content = base64.b64decode(self.metadata[resource_type + "_custom"]["content"])
        return file_content

    def set_new_parameters_to_node(self, new_data, new_node, parent_name, parent_type):
        disallowed_fields = self.build_disallowed_fields(parent_type)
        new_node.__setattr__(parent_type, parent_name)
        for field in new_data:
            if field['name'] != '' and field['value'] != '':
                if not field['name'].startswith('_') and field['name'] not in disallowed_fields:
                    new_node.__setattr__(field['name'], field['value'])

    def create_new_node(self, resource_name, resource_type, root):
        new_node = AnyNode(
            name=resource_name,
            type=resource_type,
            parent=root,
            source="custom"
        )
        return new_node

    def generate_tree_from_file_content(self, file_content):
        file_content = json.loads(file_content)
        logger.info("Importing from the file content")
        importer = DictImporter()
        root = importer.import_(file_content)
        return root

    def build_disallowed_fields(self, parent_type):
        disallowed_fields = ['name', 'type', 'source', 'parent', 'children', 'leaves', 'root', 'separator',
                             'is_leaf', 'is_root', 'descendants', 'height', 'iter_path_reverse', 'path',
                             'depth', 'ancestors', parent_type]
        return disallowed_fields

    def clear_metadata_from_session(self):
        logger.info("Clearing session metadata")
        self.metadata = {}

    def get_metadata_from_session(self):
        logger.info("Getting metadata from session")
        return self.metadata

    def get_resource_by_type_json(self, parent_type=None, parent_name=None, resource_type=None, resource_name=None):
        allowed_resources = ['codec', 'controller', 'video_system', 'loan', 'part', 'pc', 'projector', 'room']

        if resource_type and resource_type not in allowed_resources:
            logger.error("Resource type must be one of: {}".format(allowed_resources))

        # Get all session elements that start with "resource_name"
        # There should be 2: the infoream one and the custom one
        exported_last_update, exported_results = self.generate_json_for_file(parent_name, parent_type, resource_name,
                                                                             resource_type, 'infoream')

        custom_last_update, custom_results = self.generate_json_for_file(parent_name, parent_type, resource_name,
                                                                         resource_type, 'custom')

        result_to_export = {
            'lastUpdated': exported_last_update,
            'lastUpdatedCustom': custom_last_update,
            'data': exported_results,
            'custom': custom_results
        }

        return json.dumps(result_to_export)

    def generate_json_for_file(self, parent_name, parent_type, resource_name, resource_type, file_suffix):
        logger.info("Getting content from file: {}".format(resource_type + '_' + file_suffix))
        # Get the resource from the local metadata
        try:
            file_content = base64.b64decode(self.metadata[resource_type + "_" + file_suffix]["content"])
        except (TypeError, KeyError) as e:
            logger.warning("Unable to find key {} in the current metadata. (ERROR: {})".format(resource_type, e))
            return None, None

        exported_last_update = None
        exported_results = []
        try:
            all_resources, root = self.load_resources_from_file(file_content, parent_name, parent_type, resource_name,
                                                                resource_type)
            search_root = AnyNode(name="search_results")
            try:
                last_updated = find(root, lambda node: node.type == "datetime")

                for result in all_resources:
                    result.parent = search_root

                exporter = DictExporter()
                exported_results = exporter.export(search_root)
                exported_last_update = exporter.export(last_updated)

            except AttributeError as e:
                logger.error("Unable to get the datetime attribute (ERROR: {})".format(e))

        except JSONDecodeError as e:
            logger.warning(
                "Unable to retrieve the JSON content of file {} (ERROR: {})".format(resource_type + '_' + file_suffix,
                                                                                    e))

        return exported_last_update, exported_results

    def load_resources_from_file(self, file_content, parent_name, parent_type, resource_name, resource_type):
        file_content = json.loads(file_content)
        # logger.info(file_content)
        all_resources = []
        importer = DictImporter()
        # Search for parent node
        logger.info("Generating JSON tree")
        root = importer.import_(file_content)
        try:
            if resource_type and resource_name:
                logger.info("Get all resources by type '{}' and name '{}'".format(resource_type, resource_name))
                # Get all resources by type and name. Also returns resources that start with the given resource_name
                all_resources = findall(root, lambda node: (node.type == resource_type and (
                            node.name == resource_name or node.name.startswith(resource_name))))
            elif parent_name and parent_type:
                logger.info("Get all resources by parent_name and parent_type")
                # Get al resources from parent with name

                all_resources = findall(root, lambda node: (
                        node.type == resource_type and node.__getattribute__(parent_type) == parent_name))
            else:
                # Get all resources by type
                logger.info("Get all resources by resource_type: '{}'".format(resource_type))
                all_resources = findall(root, lambda node: node.type == resource_type)
        except AttributeError as e:
            logger.error("Unable to find attribute in file (ERROR: {})".format(e))
        return all_resources, root


@cache.memoize(timeout=43200)
def get_resource(parent_type=None, parent_name=None, resource_type=None, resource_name=None):
    logger.debug("Calling non cached resource")
    client = get_metadata_client(resource_type)

    if not resource_type:
        return 'A resource type is required', 400
    else:
        logger.info("Getting resource by type")
        results = client.get_resource_by_type_json(parent_type=parent_type,
                                                   parent_name=parent_name,
                                                   resource_type=resource_type,
                                                   resource_name=resource_name)
        return json.loads(results)


def set_resource(parent_type=None, parent_name=None, resource_type=None, resource_name=None, new_data=None):
    logger.debug("Calling non cached resource")

    cache.delete_memoized(get_resource, resource_type)

    client = get_metadata_client(resource_type)

    if not resource_type:
        return 'A resource type is required', 400
    else:
        logger.info("Setting resource by type")
        result = client.set_resource_by_type_json(parent_type=parent_type,
                                                  parent_name=parent_name,
                                                  resource_type=resource_type,
                                                  resource_name=resource_name,
                                                  new_data=new_data)
        return result, 201
