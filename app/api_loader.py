from app.api.private.v1.load_api import load_api_namespaces as load_api_private_namespaces
from app.api.public.v1.load_api import load_api_namespaces as load_api_public_namespaces


def load_api_private_blueprints():
    """
    Loads all the private blueprints, adding the development ones if needed
    :param app: Flask application instance
    :return: A tuple with all the blueprints
    """
    all_blueprints = (
        load_api_private_namespaces(),
    )

    return all_blueprints


def load_api_public_blueprints():
    """
    Loads all the public blueprints, adding the development ones if needed
    :param app: Flask application instance
    :return: A tuple with all the blueprints
    """
    all_blueprints = (
        load_api_public_namespaces(),
    )

    return all_blueprints
