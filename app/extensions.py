from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_caching import Cache
from flask_jwt_extended import JWTManager

#
# Cahing: By default, caching is disabled, but this can be changed using the configuration parameters in config
#
cache = Cache(config={'CACHE_TYPE': 'null'})
# Revoked token blacklist
# In production this will be redis
jwt = JWTManager()
#
# Database
#
db = SQLAlchemy()
migrate = Migrate()
