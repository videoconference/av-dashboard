import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { fetchInforeamData } from '../../actions/metadata';
import { Icon, Message, Segment, Form } from 'semantic-ui-react';

export default function FetchInforeamForm() {
  const dispatch = useDispatch();
  const [fetching, setFetching] = useState(false);
  const [result, setResult] = useState(false);
  const [value, setValue] = useState(false);

  const handleChange = (e, { value }) => setValue(value);

  const options = [
    { key: 1, text: 'Codecs', value: 'codec' },
    { key: 2, text: 'Controllers', value: 'controller' },
    { key: 3, text: 'Loans', value: 'loan' },
    { key: 4, text: 'Parts', value: 'part' },
    { key: 5, text: 'Pcs', value: 'pc' },
    { key: 6, text: 'Projectors', value: 'projector' },
    { key: 7, text: 'Rooms', value: 'room' },
    { key: 8, text: 'Video Systems', value: 'video_system' }
  ];

  const fetchInfoream = async () => {
    setFetching(true);
    // Fetch
    const response = await dispatch(fetchInforeamData(value));
    if (response && response.result === 'OK') {
      console.log('Set result');
      setResult(response);
    } else {
      console.log('Set result error');
      setResult({
        result: 'ERROR',
        message: 'Unable to update from Infoream'
      });
    }

    setFetching(false);
  };

  return (
    <Segment>
      <Form>
        <Form.Field>
          <label>Fetch the data from Infoream</label>
          {result && (
            <Message color={result.result === 'OK' ? 'green' : 'red'}>
              {result.message}
            </Message>
          )}
          <Form.Dropdown
            disabled={fetching}
            onChange={handleChange}
            options={options}
            placeholder="Choose an option"
            selection
            value={value}
          />
          <Form.Button
            disabled={!value}
            loading={fetching}
            onClick={fetchInfoream}
          >
            Re-fetch the Infoream data
          </Form.Button>

          <Message>
            <Icon name="help" /> Fetches the data from Infoream manually for the
            selected device type.
          </Message>
        </Form.Field>
      </Form>
    </Segment>
  );
}
