import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { clearMetadataCache } from '../../actions/metadata';
import { Button, Icon, Message, Segment, Form } from 'semantic-ui-react';

export default function InvalidateCacheForm() {
  const dispatch = useDispatch();
  const [fetching, setFetching] = useState(false);
  const [result, setResult] = useState(false);

  const invalidateCache = async () => {
    setFetching(true);
    // Fetch
    const response = await dispatch(clearMetadataCache());
    if (response && response.result === 'OK') {
      console.log("Set result")
      setResult(response);
    } else {
      console.log("Set result error")
      setResult({
        result: 'ERROR',
        message: 'Unable to invalidate cache. Check the server logs'
      });
    }

    setFetching(false);
  };

  return (
    <Segment>
      <Form>
        <Form.Field>
          <label>Invalidate the application cache</label>
          {result && (
            <Message
              color={result.result === 'OK'? 'green': 'red'}
            >
              {result.message}
            </Message>
          )}
          <Button loading={fetching} onClick={invalidateCache}>
            Invalidate
          </Button>
          <Message>
            <Icon name="help" /> This button removes from the application cache
            all the items stored. This is useful when a change is made on
            Infoream and we want the changes to be reflected in the applications
            data.
          </Message>
        </Form.Field>
      </Form>
    </Segment>
  );
}
