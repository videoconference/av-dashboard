import React from 'react';
import MyResponsivePie from './MyResponsivePie';
import MyResponsiveBar from './MyResponsiveBar';

/**
 * Generates either a Pie or a Bar chart depending on the parameter passed.
 * @param {string} type - pie|bar
 * @param {Array} data - Array with the objects to generate the charts
 */
export default function MyResponsiveGraph({ type, data }) {

  if (type === 'pie') {
    return <MyResponsivePie data={data} />;
  }

  if (type === 'bar'){
    return <MyResponsiveBar data={data}/>
  }

  return null;
}
