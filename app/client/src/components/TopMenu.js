import React from 'react';
import { Menu, Container } from 'semantic-ui-react';
import { useAuthentication } from '../auth';
import { useSelector } from 'react-redux';
import { isLoggedIn } from '../selectors/auth';
import { getUserInfo } from '../selectors/user';
import DevBanner from './DevBanner';
/**
 * Display the CERN Toolbar
 * INfo: https://drupal-tools.web.cern.ch/docs/modules/cern-toolbar
 */
export default function TopMenu() {
  const isUserLoggedIn = useSelector(isLoggedIn);
  const user = useSelector(getUserInfo);
  const { login, logout } = useAuthentication();

  if (isUserLoggedIn) {
    return (
      <Menu inverted fixed="top" style={{ display: 'block' }}>
        <Container fluid>
          <Menu.Item><strong>CERN</strong></Menu.Item>
          <Menu.Item name="Accelerating Science" />
          <Menu.Menu position="right">
            <Menu.Item name="loggedin">
            Signed in as: {user ? user.uid : ''}
            </Menu.Item>
            <Menu.Item name="logout" onClick={logout}>
              Logout
            </Menu.Item>
            <Menu.Item as={'a'} href="https://cern.ch/directory" name="directory">Directory</Menu.Item>
          </Menu.Menu>
        </Container>
        <Container fluid>
          <DevBanner />
        </Container>
      </Menu>
    );
  }

  return (
    <Menu inverted fixed="top" style={{ display: 'block' }}>
      <Container fluid>
        <Menu.Item name="CERN" />
        <Menu.Menu position="right">
          <Menu.Item name="signup" onClick={login}>
            Login
          </Menu.Item>
        </Menu.Menu>
      </Container>
      <Container fluid>
        <DevBanner />
      </Container>
    </Menu>
  );
}
