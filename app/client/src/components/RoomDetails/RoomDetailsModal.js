import React from 'react';
import {
  Modal,
  Header,
  Button,
  Icon,
  Segment,
  Grid
} from 'semantic-ui-react';
import {
  codecItemFields,
  pcItemFields,
  projectorItemFields,
  controllerItemFields
} from '../../utils/table-fields';
import ItemsTable from '../ConferenceRooms/ItemsTable';
import { getNameWithURL, getLanDBLink } from '../../utils/table-formatters';

export default function RoomDetailsModal({
  activeRoom,
  modalOpen,
  handleClose
}) {

  return (
    <Modal open={modalOpen} onClose={handleClose} size="large">
      <Header icon="browser" content={activeRoom} />
      <Modal.Content>
        <Grid columns={1}>
          <Grid.Column>
            <Segment basic>
              <Header as="h3">
                Room details <strong>{activeRoom}</strong>
              </Header>
              <Header as="h4">Codecs</Header>
              <ItemsTable
                displayFilters={false}
                parentResourceType="room"
                resourceType="codec"
                activeRoom={activeRoom}
                additionalFields={codecItemFields}
                formatters={{ name: getNameWithURL, landb: getLanDBLink }}
              />
              <Header as="h4">Projectors</Header>
              <ItemsTable
                displayFilters={false}
                parentResourceType="room"
                resourceType="projector"
                activeRoom={activeRoom}
                additionalFields={projectorItemFields}
                formatters={{ name: getNameWithURL, landb: getLanDBLink }}
              />
              <Header as="h4">PCs</Header>
              <ItemsTable
                displayFilters={false}
                parentResourceType="room"
                resourceType="pc"
                activeRoom={activeRoom}
                additionalFields={pcItemFields}
                formatters={{ name: getNameWithURL, landb: getLanDBLink }}
              />
              <Header as="h4">Controllers</Header>
              <ItemsTable
                displayFilters={false}
                parentResourceType="room"
                resourceType="controller"
                activeRoom={activeRoom}
                additionalFields={controllerItemFields}
                formatters={{ name: getNameWithURL, landb: getLanDBLink }}
              />
            </Segment>
          </Grid.Column>
        </Grid>
      </Modal.Content>
      <Modal.Actions>
        <Button color="green" onClick={handleClose} inverted>
          <Icon name="checkmark" /> Close
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
