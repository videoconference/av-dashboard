import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import _ from 'lodash';
import { DateTime } from 'luxon';

import {
  Table,
  Segment,
  Loader,
  Dimmer,
  Label,
  Icon,
  Form,
  Input,
  Grid
  // Header,
  // Grid
} from 'semantic-ui-react';
import RoomDetailsModal from '../RoomDetails/RoomDetailsModal';
import { getMetadata as getMetadataAction } from '../../actions/metadata';
import RoomsStats from './RoomsStats';
import { getSourceFormatter } from '../../utils/table-formatters';
// import MyResponsivePie from '../charts/MyResponsivePie';

/**
 *
 * @param {*} param0
 */
export default function RoomsTable({ displayModal }) {
  const dispatch = useDispatch();
  const [column, setColumn] = useState(null);
  const [data, setData] = useState([]);
  const [rawData, setRawData] = useState([]);

  const [lastUpdated, setLastUpdated] = useState(null);

  const [fetching, setFetching] = useState(false);

  const [activeRoom, setActiveRoom] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  const [modalStatsOpen, setModalStatsOpen] = useState(false);

  const [direction, setDirection] = useState(null);

  useEffect(() => {
    const fetchRooms = async () => {
      const resources = [['resourceType', 'room']];
      setFetching(true);
      const result = await dispatch(getMetadataAction(resources));

      if (result && result['data'] && result['data'].children) {
        const results = result['data'].children.filter(
          (node) => node.type === 'room'
        );

        let items = [];
        let itemCount = 1;

        function getItems(item, i) {
          item.status = getRoomStatus(item.status);
          item.category = getRoomCategory(item.category);
          item.count = itemCount;
          itemCount++;
          items.push(item);
        }

        results.forEach(getItems);

        setData(items);
        setRawData(result);

        setLastUpdated(result['lastUpdated']);
        setFetching(false);
      }
    };

    fetchRooms();
  }, [dispatch]);

  const handleSort = (clickedColumn) => () => {
    if (column !== clickedColumn) {
      setColumn(clickedColumn);
      setData(_.sortBy(data, [clickedColumn]));
      setDirection('ascending');

      return;
    }

    setData(data.reverse());
    setDirection(direction === 'ascending' ? 'descending' : 'ascending');
  };

  const handleClose = () => {
    setModalOpen(false);
    setActiveRoom(null);
  };

  const handleStatsClose = () => {
    setModalStatsOpen(false);
  };

  const selectRoom = (roomName) => {
    setActiveRoom(roomName);
    setModalOpen(true);
  };

  const openStatsModal = () => {
    setModalStatsOpen(true);
  };

  const [filter, setFilter] = useState('');
  const lowercasedFilter = filter.toLowerCase();

  const filterByText = (text) => {
    return Object.keys(text).some((key) => {
      if (text[key] !== null) {
        return text[key].toString().toLowerCase().includes(lowercasedFilter);
      }

      return null;
    });
  };

  const filteredData = data.filter((text) => {
    return filterByText(text);
  });

  const handleFilterChange = (event) => {
    setFilter(event.target.value);
  };

  const getRoomCategory = (name) => {
    let category = '';
    switch (name) {
      case 'AV-VCR':
        category = 'Videoconference';
        break;
      case 'AV-MRO':
        category = 'Meeting';
        break;
      case 'AV-VIS':
        category = 'Visit';
        break;
      default:
        category = 'Other';
    }
    return category;
  };

  const getRoomStatus = (name) => {
    switch (name) {
      case 'I':
        return 'Supported';
      case 'IE':
        return 'Design';

      default:
        return name;
    }
  };

  if (fetching) {
    return (
      <Segment basic>
        <Dimmer active inverted>
          <Loader active />
        </Dimmer>
      </Segment>
    );
  }

  return (
    <Grid columns={1}>
      <Grid.Row>
        <Grid.Column floated="left" width={5}>
          <Form>
            <Form.Group inline>
              <Form.Field>
                <label>Filter Rooms: </label>
                <Input
                  value={filter}
                  placeholder="ex: 28/1"
                  onChange={handleFilterChange}
                />
              </Form.Field>
            </Form.Group>
          </Form>
        </Grid.Column>
        <Grid.Column floated="right" width={6}>
          <Label>
            {data.length > 0 && (
              <>
                <Icon name="clock" /> Last updated{' '}
                {DateTime.fromISO(lastUpdated.value, { zone: 'UTC' })
                  .toLocal()
                  .toLocaleString(DateTime.DATETIME_FULL)}
              </>
            )}
          </Label>
          <Label color="blue" as={'a'} onClick={openStatsModal}>
            <Icon name="pie graph" /> View Rooms Stats
          </Label>
        </Grid.Column>
      </Grid.Row>

      <Grid.Row>
        <Grid.Column>
          <p>Total number: {filteredData.length}</p>
        </Grid.Column>
      </Grid.Row>

      <Grid.Row>
        <Grid.Column>
          <Table sortable celled striped selectable compact size="small">
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>#</Table.HeaderCell>
                <Table.HeaderCell
                  sorted={column === 'source' ? direction : null}
                  onClick={handleSort('source')}
                >
                  Source
                </Table.HeaderCell>
                <Table.HeaderCell
                  sorted={column === 'name' ? direction : null}
                  onClick={handleSort('name')}
                >
                  Name
                </Table.HeaderCell>
                <Table.HeaderCell
                  sorted={column === 'description' ? direction : null}
                  onClick={handleSort('description')}
                >
                  Name
                </Table.HeaderCell>
                <Table.HeaderCell
                  sorted={column === 'sponsor' ? direction : null}
                  onClick={handleSort('sponsor')}
                >
                  Sponsor
                </Table.HeaderCell>
                <Table.HeaderCell
                  sorted={column === 'status' ? direction : null}
                  onClick={handleSort('status')}
                >
                  Status
                </Table.HeaderCell>
                <Table.HeaderCell
                  sorted={column === 'category' ? direction : null}
                  onClick={handleSort('category')}
                >
                  Category
                </Table.HeaderCell>
                <Table.HeaderCell
                  sorted={column === 'commissioned' ? direction : null}
                  onClick={handleSort('commissioned')}
                >
                  Commissioned
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              {data.length > 0 &&
                filteredData.map((element, i) => (
                  <Table.Row key={i}>
                    <Table.Cell>{i + 1}</Table.Cell>
                    <Table.Cell>{getSourceFormatter(element)}</Table.Cell>
                    {displayModal && (
                      <Table.Cell
                        selectable
                        onClick={() => selectRoom(element.name)}
                      >
                        <span title="Open room modal">{element.name}</span>
                      </Table.Cell>
                    )}
                    {!displayModal && (
                      <Table.Cell>
                        <span title="Open room modal">{element.name}</span>
                      </Table.Cell>
                    )}
                    <Table.Cell>{element.description}</Table.Cell>
                    <Table.Cell>{element.sponsor}</Table.Cell>
                    <Table.Cell>{element.status}</Table.Cell>
                    <Table.Cell>{element.category}</Table.Cell>
                    <Table.Cell>{element.commissioned}</Table.Cell>
                  </Table.Row>
                ))}
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid.Row>

      {activeRoom && displayModal && (
        <RoomDetailsModal
          activeRoom={activeRoom}
          modalOpen={modalOpen}
          handleClose={handleClose}
        />
      )}
      {displayModal && (
        <RoomsStats
          modalOpen={modalStatsOpen}
          handleClose={handleStatsClose}
          data={rawData}
        />
      )}
    </Grid>
  );
}
