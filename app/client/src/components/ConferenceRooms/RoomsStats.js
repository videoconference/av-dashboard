import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import {
  Segment,
  Loader,
  Dimmer,
  Header,
  Grid,
  Modal,
  Icon,
  Button
} from 'semantic-ui-react';
import MyResponsivePie from '../charts/MyResponsivePie';
import MyResponsiveBar from '../charts/MyResponsiveBar';

/**
 *
 * @param {*} param0
 */
export default function RoomsStats({ modalOpen, handleClose, data }) {
  const dispatch = useDispatch();
  const [fetching, setFetching] = useState(false);
  const [categories] = useState([]);
  const [sponsors] = useState([]);
  const [years] = useState([]);
  const [statuses] = useState([]);

  useEffect(() => {
    const handleSponsors = (item) => {
      let result = sponsors.filter((obj) => {
        return obj.label === item.sponsor;
      });

      if(!result[0] && item.sponsor === null){
        result = sponsors.filter((obj) => {
          return obj.label === 'unknown';
        });
      }

      if (result[0]) {
        result[0].value++;
      } else {
        let newSponsor = {};
        if (item.sponsor === null) {
          newSponsor.id = 'unknown';
          newSponsor.label = 'unknown';
        } else {
          newSponsor.id = item.sponsor;
          newSponsor.label = item.sponsor;
        }
        newSponsor.value = 1;
        sponsors.push(newSponsor);
      }
    };

    const handleStatuses = (item) => {
      var result = statuses.filter((obj) => {
        return obj.label === item.status;
      });

      if (result[0]) {
        result[0].value++;
      } else {
        let newStatus = {};
        newStatus.id = item.status;
        newStatus.label = item.status;
        newStatus.value = 1;
        statuses.push(newStatus);
      }
    };

    const handleYears = (item) => {
      var result = years.filter((obj) => {
        return obj.label === item.commissioned.split('-')[0];
      });

      if (result[0]) {
        result[0].value++;
      } else {
        let newYear = {};
        newYear.id = item.commissioned.split('-')[0];
        newYear.label = item.commissioned.split('-')[0];
        newYear.value = 1;
        years.push(newYear);
      }
    };

    const getRoomCategory = (name) => {
      var result = categories.filter((obj) => {
        return obj.label === name;
      });

      if (result[0]) {
        result[0].value++;
      } else {
        let newCategory = {};
        newCategory.id = name;
        newCategory.label = name;
        newCategory.value = 1;
        categories.push(newCategory);
      }

      return name;
    };

    const fetchRooms = async () => {
      const result = data;

      if (result && result['data'] && result['data'].children) {
        const results = result['data'].children;

        let items = [];
        let itemCount = 1;

        function getItems(item, i) {
          item.category = getRoomCategory(item.category);
          item.count = itemCount;
          itemCount++;
          items.push(item);

          handleSponsors(item);
          handleYears(item);
          handleStatuses(item);
        }

        results.forEach(getItems);
      }

      setFetching(false);
    };

    fetchRooms();
  }, [dispatch, categories, sponsors, years, statuses, data]);

  if (fetching) {
    return (
      <Segment basic>
        <Dimmer active inverted>
          <Loader active />
        </Dimmer>
      </Segment>
    );
  }

  return (
    <Modal open={modalOpen} onClose={handleClose} size="large">
      <Header icon="pie graph" content="Rooms Stats" />
      <Modal.Content>
        <Grid columns={2}>
          <Grid.Column>
            <Segment style={{ height: 400 }}>
              <Header as={'h2'}>By Category</Header>
              <MyResponsivePie data={categories} />
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment style={{ height: 400 }}>
              <Header as={'h2'}>By Sponsor</Header>
              <MyResponsivePie data={sponsors} />
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment style={{ height: 400 }}>
              <Header as={'h2'}>By Year</Header>
              <MyResponsiveBar
                data={[].concat(years).sort((a, b) => (a.id > b.id ? 1 : -1))}
              />
            </Segment>
          </Grid.Column>
          <Grid.Column>
            <Segment style={{ height: 400 }}>
              <Header as={'h2'}>By Status</Header>
              <MyResponsivePie data={statuses} />
            </Segment>
          </Grid.Column>
        </Grid>
      </Modal.Content>
      <Modal.Actions>
        <Button color="green" onClick={handleClose} inverted>
          <Icon name="checkmark" /> Close
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
