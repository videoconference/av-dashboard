import React from 'react';
import ItemsTable from './ItemsTable';
import KpisSection from './KpisSection';
import {
  getNameWithURL,
  getLanDBLink,
  getStatusFormatter,
  getSourceFormatter,
  getRoomFormatter
} from '../../utils/table-formatters';
import {
  codecItemFields,
  controllerItemFields,
  partItemFields,
  pcItemFields,
  projectorItemFields
} from '../../utils/table-fields';

export default function PrivateConferenceRoomsSection({ activeItem, user }) {
  console.log(activeItem);
  return (
    <>
      {user && user.roles.includes('admins') && (
        <>
          {activeItem === 'codecs' && (
            <ItemsTable
              user={user}
              displayFilters={true}
              parentResourceType="room"
              resourceType="codec"
              additionalFields={codecItemFields}
              formatters={{
                name: getNameWithURL,
                landb: getLanDBLink,
                status: getStatusFormatter,
                source: getSourceFormatter,
                room: getRoomFormatter
              }}
              statsAttributes={[
                ['manufacturer', 'pie'],
                ['model', 'pie'],
                ['commissioned', 'bar']
              ]}
            />
          )}
          {activeItem === 'controllers' && (
            <ItemsTable
              user={user}
              displayFilters={true}
              parentResourceType="room"
              resourceType="controller"
              additionalFields={controllerItemFields}
              formatters={{
                name: getNameWithURL,
                landb: getLanDBLink,
                status: getStatusFormatter,
                source: getSourceFormatter,
                room: getRoomFormatter
              }}
              statsAttributes={[
                ['manufacturer', 'pie'],
                ['model', 'pie'],
                ['commissioned', 'bar']
              ]}
            />
          )}
          {activeItem === 'kpis' && <KpisSection />}
          {activeItem === 'pcs' && (
            <ItemsTable
              user={user}
              displayFilters={true}
              parentResourceType="room"
              resourceType="pc"
              additionalFields={pcItemFields}
              formatters={{
                name: getNameWithURL,
                landb: getLanDBLink,
                status: getStatusFormatter,
                source: getSourceFormatter,
                room: getRoomFormatter
              }}
              statsAttributes={[
                ['manufacturer', 'pie'],
                ['model', 'pie'],
                ['description', 'pie'],
                ['status', 'pie'],
                ['commissioned', 'bar']
              ]}
            />
          )}
          {activeItem === 'projectors' && (
            <ItemsTable
              user={user}
              displayFilters={true}
              parentResourceType="room"
              resourceType="projector"
              additionalFields={projectorItemFields}
              formatters={{
                landb: getLanDBLink,
                status: getStatusFormatter,
                source: getSourceFormatter,
                room: getRoomFormatter
              }}
              statsAttributes={[
                ['manufacturer', 'pie'],
                ['model', 'pie'],
                ['status', 'pie'],
                ['commissioned', 'bar']
              ]}
            />
          )}
          {activeItem === 'parts' && (
            <ItemsTable
              user={user}
              displayFilters={true}
              parentResourceType="room"
              resourceType="part"
              additionalFields={partItemFields}
              formatters={{
                room: getRoomFormatter,
                source: getSourceFormatter
              }}
              statsAttributes={[['description', 'pie']]}
            />
          )}
          {activeItem === 'video-systems' && (
            <ItemsTable
              user={user}
              displayFilters={true}
              parentResourceType="room"
              resourceType="video_system"
              additionalFields={pcItemFields}
              formatters={{
                name: getNameWithURL,
                landb: getLanDBLink,
                status: getStatusFormatter,
                source: getSourceFormatter,
                room: getRoomFormatter
              }}
              statsAttributes={[
                ['manufacturer', 'pie'],
                ['model', 'pie'],
                ['description', 'pie'],
                ['status', 'pie'],
                ['commissioned', 'bar']
              ]}
            />
          )}
        </>
      )}
    </>
  );
}
