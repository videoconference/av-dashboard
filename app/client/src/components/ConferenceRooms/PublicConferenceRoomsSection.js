import React from 'react';
import RoomsTable from './RoomsTable';
import ItemsTable from './ItemsTable';
import { loanItemFields } from '../../utils/table-fields';
import { getSourceFormatter } from '../../utils/table-formatters';

export default function PublicConferenceRoomsSection({activeItem, user}) {
  return (
    <>
      {activeItem === 'rooms' && (
        <RoomsTable displayModal={user && user.roles.includes('admins')} />
      )}
      {activeItem === 'loans' && (
        <ItemsTable
          user={user}
          displayFilters={true}
          parentResourceType="loan_category"
          resourceType="loan"
          additionalFields={loanItemFields}
          formatters={{
            source: getSourceFormatter
          }}
          statsAttributes={[
            ['loan_category', 'pie'],
            ['manufacturer', 'pie'],
            ['model', 'pie'],
            ['status', 'pie']
          ]}
        />
      )}
    </>
  );
}
