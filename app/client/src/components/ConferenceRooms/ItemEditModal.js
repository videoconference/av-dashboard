import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import {
  Segment,
  Loader,
  Dimmer,
  Header,
  Modal,
  Icon,
  Button,
  Form,
  Input,
  Message,
  List
} from 'semantic-ui-react';
import {
  setMetadata as setMetadataAction,
  getMetadata as getMetadataAction
} from '../../actions/metadata';

/**
 * Component that displays a Modal for editing an Item.
 * The values of the form will be sent to the backend and stored as "custom" values for the current item
 * @param {Object} item - Item that will be modified
 * @param {boolean} modalOpen - Whether the modal must be displayed or not
 * @param {Function} handleClose - Function that will be triggered when the modal is closed
 * @param {string} parentResourceType - The type of the parent node for the current item
 * @param {string} parentResourceName - The name of the parent node for the current item
 */
export default function ItemEditModal({
  item,
  modalOpen,
  handleClose,
  parentResourceType,
  parentResourceName
}) {
  const dispatch = useDispatch();
  const [fetching, setFetching] = useState(false);
  const [inputList, setInputList] = useState([{ name: '', value: '' }]);
  const [editSuccess, setEditSuccess] = useState(null);
  const [editResult, setEditResult] = useState(null);

  useEffect(() => {
    const fetchData = async () => {
      const resources = [
        ['resourceType', item.type],
        ['parentType', parentResourceType],
        ['resourceName', item.name],
        ['parentName', parentResourceName]
      ];
      setFetching(true);
      const result = await dispatch(getMetadataAction(resources));
      if (result && result['custom'] && result['custom'].children) {
        const results = result['custom'].children.filter(
          (node) => node.name === item.name && node.type === item.type
        );

        const list = [];
        const disallowedKeys = ['name', 'type', 'source', parentResourceType];
        for (var key in results[0]) {
          if (!disallowedKeys.includes(key)) {
            const newValue = { name: key, value: results[0][key] };
            console.log(newValue);
            list.push(newValue);
          }
        }
        list.push({ name: '', value: '' });
        setInputList(list);
      }

      setFetching(false);
    };

    fetchData();
  }, [dispatch, item, parentResourceName, parentResourceType]);

  /**
   * Saves the current form fields making an API call
   */
  const saveData = () => {
    console.log('Saving data');
    const saveDForm = async () => {
      const resources = [
        ['resourceType', item.type],
        ['parentType', parentResourceType],
        ['resourceName', item.name],
        ['parentName', parentResourceName]
      ];
      setFetching(true);
      setEditSuccess(null);
      setEditResult(null);
      const result = await dispatch(setMetadataAction(resources, inputList));
      console.log(result);
      if (result.result && result.result === 'OK') {
        setEditSuccess(true);
      } else {
        setEditSuccess('error');
      }
      setEditResult(result);
      setFetching(false);
    };
    saveDForm();
  };

  /**
   * handle input change
   * @param {Event} e - Event that has been triggered
   * @param {Number} index - Index of the field that has changed in the inputList
   */
  const handleInputChange = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputList];
    list[index][name] = value;
    setInputList(list);
  };

  /**
   * handle click event of the Remove button
   * @param {Number} index - Index of the field in the inputList array that is going to be removed
   */
  const handleRemoveClick = (index) => {
    const list = [...inputList];
    list.splice(index, 1);
    setInputList(list);
  };

  /**
   * handle click event of the Add button
   */
  const handleAddClick = () => {
    setInputList([...inputList, { name: '', value: '' }]);
  };

  return (
    <Modal open={modalOpen} onClose={handleClose} size="large">
      <Header
        icon="edit"
        content="Edit Custom Data"
        subheader={`${item.type} ${item.name}`}
      />
      <Modal.Content>
        <Header as="h4" content="Infoream values" />
        <List horizontal>
          {Object.keys(item).map((key, index) => (
            <List.Item key={`infor-${index}`}>
              <List.Content>
                <List.Header>{key}</List.Header>
                {key === 'custom'? '-' : (item[key])}
              </List.Content>
            </List.Item>
          ))}
        </List>

        <Header as="h4" content="Custom values" />
        <Message>
          <Message.Header>Convention for names</Message.Header>
          <Message.List>
            <Message.Item>
              Use <strong>snake case</strong> for the attributes names:
              "variable_name", "webcast_url", etc.
            </Message.Item>
            <Message.Item>
              <strong>DO NOT</strong> use uppercase letters
            </Message.Item>
          </Message.List>
        </Message>
        {editSuccess === true && (
          <Message
            success
            header="Success setting up the custom values"
            content={JSON.stringify(editResult)}
          />
        )}
        {editSuccess === 'error' && (
          <Message
            negative
            header="Error setting the custom values"
            content={JSON.stringify(editResult)}
          />
        )}
        {fetching && (
          <Segment basic>
            <Dimmer active inverted>
              <Loader active />
            </Dimmer>
          </Segment>
        )}
        {!fetching && (
          <>
            <Form>
              {inputList.map((x, i) => {
                return (
                  <Form.Group widths="equal" key={`cform-${i}`}>
                    <Form.Field
                      control={Input}
                      label="Attribute name"
                      onChange={(e) => handleInputChange(e, i)}
                      name="name"
                      value={x.name}
                    />
                    <Form.Field
                      control={Input}
                      label="Attribute value"
                      placeholder="Enter Attribute Value"
                      onChange={(e) => handleInputChange(e, i)}
                      name="value"
                      value={x.value}
                    />

                    {inputList.length !== 1 && (
                      <Button
                        color="red"
                        size="small"
                        onClick={() => handleRemoveClick(i)}
                      >
                        Remove Field
                      </Button>
                    )}
                    {inputList.length - 1 === i && (
                      <Button onClick={handleAddClick}>Add Field</Button>
                    )}
                  </Form.Group>
                );
              })}
              <pre>{JSON.stringify(inputList)}</pre>
              <Form.Group>
                <Button onClick={saveData} type="button">
                  <Icon name="save" /> Save
                </Button>
              </Form.Group>
            </Form>
          </>
        )}
      </Modal.Content>
      <Modal.Actions>
        <Button color="red" onClick={handleClose} inverted>
          <Icon name="cancel" /> Close
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
