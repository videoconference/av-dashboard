import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import {
  Segment,
  Loader,
  Dimmer,
  Header,
  Grid,
  Modal,
  Icon,
  Button
} from 'semantic-ui-react';
import MyResponsiveGraph from '../charts/MyResponsiveGraph';

/**
 *
 * @param {*} param0
 */
export default function ItemsStats({
  modalOpen,
  handleClose,
  statsAttributes,
  resourceType,
  parentResourceType,
  data
}) {
  const dispatch = useDispatch();
  const [fetching, setFetching] = useState(false);
  let [stats] = useState([]);

  useEffect(() => {
    const handleStats = (item, attribute) => {
      if (item[attribute] === null) {
        item[attribute] = 'unknown';
      }

      if (attribute === 'commissioned') {
        item[attribute] = item.commissioned.split('-')[0];
      }

      let result = stats[attribute]
        ? stats[attribute].filter((obj) => {
            return obj.label === item[attribute];
          })
        : 'unknown';

      if (result && result[0]) {
        result[0].value++;
      } else {
        let newAttribute = {};

        newAttribute.id = item[attribute];
        newAttribute.label = item[attribute];

        newAttribute.value = 1;
        newAttribute[attribute] = item[attribute];

        stats[attribute].push(newAttribute);
      }
    };

    const processData = () => {
      setFetching(true);
      const result = data;

      if (result && result['data'] && result['data'].children) {
        const results = result['data'].children.filter(
          (node) => node.type === resourceType
        );

        function getItems(item) {
          if (statsAttributes) {
            statsAttributes.forEach((attribute) => {
              handleStats(item, attribute[0]);
            });
          }
        }

        results.forEach(getItems);
      }
      setFetching(false);
    };

    if (statsAttributes) {
      statsAttributes.forEach((attribute) => {
        stats[attribute[0]] = [];
      });
    }

    processData();
  }, [
    dispatch,
    stats,
    statsAttributes,
    resourceType,
    parentResourceType,
    data
  ]);

  if (fetching) {
    return (
      <Segment basic>
        <Dimmer active inverted>
          <Loader active />
        </Dimmer>
      </Segment>
    );
  }

  return (
    <Modal open={modalOpen} onClose={handleClose} size="large">
      <Header icon="pie graph" content="Stats" />
      <Modal.Content>
        <Grid columns={2}>
          {stats &&
            statsAttributes &&
            statsAttributes.map((element, i) => (
              <Grid.Column key={`stats-${i}`}>
                <Segment style={{ height: 400 }}>
                  <Header as={'h2'}>By {element[0]}</Header>
                  <MyResponsiveGraph
                    data={[]
                      .concat(stats[element[0]])
                      .sort((a, b) => (a.id > b.id ? 1 : -1))}
                    type={element[1]}
                  />
                </Segment>
              </Grid.Column>
            ))}
        </Grid>
      </Modal.Content>
      <Modal.Actions>
        <Button color="green" onClick={handleClose} inverted>
          <Icon name="checkmark" /> Close
        </Button>
      </Modal.Actions>
    </Modal>
  );
}
