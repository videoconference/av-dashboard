import React, { useEffect, useState } from 'react';
import { Grid, Header, Segment, Statistic, Dimmer, Loader } from 'semantic-ui-react';
import MyResponsivePie from '../charts/MyResponsivePie';
import { useDispatch } from 'react-redux';
import { getStats as getStatsAction } from '../../actions/stats';


export default function KpisSection() {
  const [data, setData] = useState({});
  const [fetching, setFetching] = useState(false);
  const dispatch = useDispatch();

  const buildMeetingRoomsCoverage = () => {
    return [
      {
        id: 'managed_rooms',
        label: 'Managed Rooms',
        value: data.meetingRoomsManaged
      },
      {
        id: 'unmanaged_rooms',
        label: 'Unmanaged rooms',
        value: data.meetingRooms - data.meetingRoomsManaged
      }
    ];
  };

  useEffect(() => {
    const fetchItems = async () => {
      setFetching(true);
      const result = await dispatch(getStatsAction());
      if (result && result['data']) {
        setData(result['data']);
      }
      setFetching(false);
    };

    fetchItems();
  }, [dispatch]);

  if (fetching) {
    return (
      <Segment basic>
        <Dimmer active inverted>
          <Loader active />
        </Dimmer>
      </Segment>
    );
  }

  if (!data && !fetching) {
    return <p>Unable to fetch KPI's</p>;
  }

  return (
    <>
      <Header as={'h2'}>Service KPI's</Header>
      <Grid columns={2}>
        <Grid.Column>
          <Segment style={{ height: 400 }}>
            <Header as={'h3'}>Service Coverage</Header>
            <MyResponsivePie data={buildMeetingRoomsCoverage()} />
          </Segment>
        </Grid.Column>
        <Grid.Column>
          <Grid columns={2}>
            <Grid.Column>
              <Statistic>
                <Statistic.Value>{data.equipmentInventiored} %</Statistic.Value>
                <Statistic.Label>Equipment Inventiored</Statistic.Label>
              </Statistic>
            </Grid.Column>
            <Grid.Column>
              <Statistic>
                <Statistic.Value>
                  {data.equipmentWithManufacturer} %
                </Statistic.Value>
                <Statistic.Label>Equipment with Manufacturer</Statistic.Label>
              </Statistic>
            </Grid.Column>
            <Grid.Column>
              <Statistic>
                <Statistic.Value>
                  {data.equipmentWithSerialNumber} %
                </Statistic.Value>
                <Statistic.Label>Equipment with Serial Number</Statistic.Label>
              </Statistic>
            </Grid.Column>
          </Grid>
        </Grid.Column>
      </Grid>
    </>
  );
}
