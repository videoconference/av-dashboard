import React, { useState, useEffect } from 'react';
import _ from 'lodash';
import { DateTime } from 'luxon';

import {
  Table,
  Segment,
  Loader,
  Dimmer,
  Label,
  Icon,
  Form,
  Input,
  Button,
  Grid
} from 'semantic-ui-react';
import { useDispatch } from 'react-redux';
import { getMetadata as getMetadataAction } from '../../actions/metadata';
import ItemsStats from './ItemsStats';
import ItemEditModal from './ItemEditModal';

export default function ItemsTable({
  displayFilters,
  activeRoom,
  resourceType,
  additionalFields,
  parentResourceType,
  formatters,
  statsAttributes,
  statsType,
  user
}) {
  const dispatch = useDispatch();
  const [data, setData] = useState([]);
  const [rawData, setRawData] = useState([]);

  const [lastUpdated, setLastUpdated] = useState(null);
  const [fetching, setFetching] = useState(false);

  // Filters
  const [filter, setFilter] = useState('');
  const lowercasedFilter = filter.toLowerCase();
  // Sorting
  const [column, setColumn] = useState(null);
  const [direction, setDirection] = useState(null);

  // Stats
  const [modalStatsOpen, setModalStatsOpen] = useState(false);

  const handleStatsClose = () => {
    setModalStatsOpen(false);
  };

  const openStatsModal = () => {
    setModalStatsOpen(true);
  };

  // Edit modal
  const [modalEditOpen, setModalEditOpen] = useState(false);
  const [selectedEditItem, setSelectedEditItem] = useState(false);

  const openEditModal = (item) => {
    setModalEditOpen(true);
    setSelectedEditItem(item);
  };

  const handleEditClose = () => {
    setModalEditOpen(false);
    setSelectedEditItem(null);
  };

  const filterByText = (text) => {
    const result = Object.keys(text).some((key) => {
      if (text[key] !== null) {
        return text[key].toString().toLowerCase().includes(lowercasedFilter);
      }

      return null;
    });

    return result;
  };

  const filteredData = data.filter((text) => {
    return filterByText(text);
  });

  const handleFilterChange = (event) => {
    setFilter(event.target.value);
  };

  const handleSort = (clickedColumn) => () => {
    if (column !== clickedColumn) {
      setColumn(clickedColumn);
      setData(_.sortBy(data, [clickedColumn]));
      setDirection('ascending');

      return;
    }

    setData(data.reverse());
    setDirection(direction === 'ascending' ? 'descending' : 'ascending');
  };

  useEffect(() => {
    const fetchItems = async () => {
      const activeRoomFilter = activeRoom
        ? ['parentName', `${activeRoom}`]
        : [];

      const activeRoomNameFilter = activeRoom ? ['parentType', 'room'] : [];

      const resources = [
        ['resourceType', resourceType],
        activeRoomFilter,
        activeRoomNameFilter
      ];
      setFetching(true);
      console.log(`Fetching ${resourceType}`);
      const result = await dispatch(getMetadataAction(resources));
      if (result && result['data'] && result['data'].children) {
        const results = result['data'].children;

        let items = [];
        let itemCount = 1;

        function getItems(item) {
          item.count = itemCount;
          itemCount++;

          let custom = false;
          if (result && result['custom'] && result['custom'].children) {
            custom = result['custom'].children.filter(
              (customItem) => customItem.name === item.name
            );
            if (custom.length > 0) {
              custom = custom[0];
              item.custom = custom;
            }
          }
          items.push(item);
        }

        results.forEach(getItems);

        setData(items);
        setRawData(result);
        setLastUpdated(result['lastUpdated']);
      }
      setFetching(false);
    };

    fetchItems();
  }, [dispatch, activeRoom, resourceType, parentResourceType]);

  if (!data && !fetching) {
    return <p>This room doesn't have '{resourceType}s'</p>;
  }

  if (fetching) {
    return (
      <Segment basic>
        <Dimmer active inverted>
          <Loader active />
        </Dimmer>
      </Segment>
    );
  }
  return (
    <Grid columns={1}>
      {displayFilters && (
        <Grid.Row>
          <Grid.Column floated="left" width={5}>
            <Form>
              <Form.Group inline>
                <Form.Field>
                  <label>Filter: </label>
                  <Input
                    value={filter}
                    placeholder="ex: 28/1"
                    onChange={handleFilterChange}
                  />
                </Form.Field>
              </Form.Group>
            </Form>
          </Grid.Column>
          <Grid.Column floated="right" width={5}>
            <Label>
              {data.length > 0 && (
                <>
                  <Icon name="clock" /> Last updated{' '}
                  {DateTime.fromISO(lastUpdated.value, { zone: 'UTC' })
                    .toLocal()
                    .toLocaleString(DateTime.DATETIME_FULL)}
                </>
              )}
            </Label>
            <Label color="blue" as={'a'} onClick={openStatsModal}>
              <Icon name="pie graph" /> View Stats
            </Label>
          </Grid.Column>
        </Grid.Row>
      )}
      <Grid.Row>
        <Grid.Column>
          <p>Total number: {filteredData.length}</p>
        </Grid.Column>
      </Grid.Row>

      {filteredData.length > 0 && (
        <Grid.Row>
          <Grid.Column>
            <Table
              sortable
              celled
              striped
              selectable
              compact
              size="small"
              textAlign="center"
            >
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>#</Table.HeaderCell>
                  {additionalFields.map((itemField, i) => (
                    <Table.HeaderCell
                      key={`header-${i}`}
                      sorted={column === itemField ? direction : null}
                      onClick={handleSort(itemField)}
                    >
                      {itemField}
                    </Table.HeaderCell>
                  ))}
                  {user && user.roles.includes('admins') && (
                    <Table.HeaderCell>Actions</Table.HeaderCell>
                  )}
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {filteredData &&
                  filteredData.map((item, i) => (
                    <Table.Row key={`item-${i}`}>
                      <Table.Cell>{i + 1}</Table.Cell>
                      {additionalFields.map((additionalField) => (
                        <Table.Cell key={`field-${additionalField}-${i}`}>
                          {formatters && formatters[additionalField]
                            ? formatters[additionalField](item)
                            : item[additionalField]}
                        </Table.Cell>
                      ))}
                      {user && user.roles.includes('admins') && (
                        <Table.Cell>
                          {' '}
                          <Button
                            size="mini"
                            basic
                            onClick={() => openEditModal(item)}
                          >
                            <Icon name="edit" />
                            Edit custom info
                          </Button>
                        </Table.Cell>
                      )}
                    </Table.Row>
                  ))}
              </Table.Body>
            </Table>
          </Grid.Column>
        </Grid.Row>
      )}
      <ItemsStats
        modalOpen={modalStatsOpen}
        handleClose={handleStatsClose}
        statsAttributes={statsAttributes}
        resourceType={resourceType}
        parentResourceType={parentResourceType}
        statsType={statsType}
        statsKeys={[]}
        data={rawData}
      />
      {selectedEditItem && (
        <ItemEditModal
          modalOpen={modalEditOpen}
          handleClose={handleEditClose}
          item={selectedEditItem}
          parentResourceType={parentResourceType}
          parentResourceName={selectedEditItem[parentResourceType]}
        />
      )}
    </Grid>
  );
}
