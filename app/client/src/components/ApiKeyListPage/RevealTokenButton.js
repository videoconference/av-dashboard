import React, { useState } from 'react';
import { Modal, Button, Form } from 'semantic-ui-react';

export default function RevealTokenButton({ apiKey }) {
  const [modalOpen, setModalOpen] = useState(false);

  const handleModalClose = () => {
    setModalOpen(false);
  };

  const handleModalOpen = () => {
    setModalOpen(true);
  };
  return (
    <Modal
      trigger={
        <Button basicfloated="right" onClick={handleModalOpen}>
          Reveal
        </Button>
      }
      open={modalOpen}
      onClose={handleModalClose}
      size="tiny"
    >
      <Modal.Header>Revoke API Key</Modal.Header>
      <Modal.Content>
        <Form>
          <Form.Field>
            <label>Access Token</label>
            <input readOnly value={apiKey.accessToken} />
          </Form.Field>
        </Form>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={handleModalClose}>Close</Button>
      </Modal.Actions>
    </Modal>
  );
}
