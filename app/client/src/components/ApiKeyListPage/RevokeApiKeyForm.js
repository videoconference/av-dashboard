import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Modal, Button } from 'semantic-ui-react';
import { revokeApiKey } from '../../actions/api_keys';
import { getApiKeys } from '../../actions/api_keys';

export function RevokeApiKeyForm({ apiKey }) {
  const dispatch = useDispatch();
  const [modalOpen, setModalOpen] = useState(false);

  const handleModalClose = () => {
    setModalOpen(false);
  };

  const handleModalOpen = () => {
    setModalOpen(true);
  };

  const revokeSubmit = async () => {
    console.log('Submitting service delete form');
    await dispatch(revokeApiKey(apiKey.id));
    dispatch(getApiKeys());
    handleModalClose();
  };

  return (
    <Modal
      trigger={
        <Button basic color="red" floated='right' onClick={handleModalOpen}>
          Revoke
        </Button>
      }
      open={modalOpen}
      onClose={handleModalClose}
      size="tiny"
    >
      <Modal.Header>Revoke API Key</Modal.Header>
      <Modal.Content>
        <p>
          Are you sure you want to revoke the API Key "
          <strong>{apiKey.name}</strong>"?
        </p>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={handleModalClose}>No</Button>
        <Button
          negative
          icon="checkmark"
          labelPosition="right"
          content="Yes"
          onClick={revokeSubmit}
        />
      </Modal.Actions>
    </Modal>
  );
}