import React, { useState } from 'react';
import { Button, Form, Modal, Grid, Header } from 'semantic-ui-react';
import { Form as FinalForm, Field } from 'react-final-form';
import { useDispatch } from 'react-redux';

import { getApiKeys } from '../../actions/api_keys';
import { addNewApiKey } from '../../actions/api_keys';

export default function AddApiKeyForm() {
  const [modalOpen, setModalOpen] = useState(false);
  const dispatch = useDispatch();

  const onFormSubmit = async values => {
    console.log('Submitting form');

    console.log(JSON.stringify(values, 0, 2));
    await dispatch(addNewApiKey(values));
    dispatch(getApiKeys());
    handleModalClose();
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  return (
    <Grid>
      <Grid.Column>
        <Modal
          trigger={
            <Button onClick={() => setModalOpen(true)} positive>
              Add a new API Key
            </Button>
          }
          open={modalOpen}
          onClose={handleModalClose}
          size="small"
        >
          <FinalForm
            onSubmit={onFormSubmit}
            render={({ handleSubmit, form, submitting, pristine, values }) => (
              <>
                <Header icon="browser" content="Add a new API Key" />
                <Modal.Content>
                  <Form onSubmit={handleSubmit}>

                  <Field name="name">
                        {({ input, meta }) => (
                          <Form.Field className={meta.error ? 'error' : ''}>
                            <label>Api Key Name</label>
                            <input {...input} type="text" />
                            {meta.error && meta.touched && (
                              <div
                                className="ui pointing above prompt label"
                                role="alert"
                                aria-atomic="true"
                              >
                                {meta.error}
                              </div>
                            )}
                          </Form.Field>
                        )}
                      </Field>
                    <pre>{JSON.stringify(values, 0, 2)}</pre>
                  </Form>
                </Modal.Content>
                <Modal.Actions>
                  <Button onClick={handleModalClose}>Cancel</Button>
                  <Button
                    onClick={handleSubmit}
                    color="green"
                    type="submit"
                    disabled={submitting || pristine}
                  >
                    Submit
                  </Button>
                </Modal.Actions>
              </>
            )}
          />
        </Modal>
      </Grid.Column>
    </Grid>
  );
}
