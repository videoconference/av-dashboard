import { applyMiddleware, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import root from './reducers';

/**
 * Creates a REDUX store with the dev tools enabled and the thunk middleware
 */
const store = createStore(
  root,
  composeWithDevTools(applyMiddleware(thunkMiddleware))
);

export default store;
