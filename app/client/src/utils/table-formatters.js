import React from 'react';
import { Label, Icon, Popup, Button } from 'semantic-ui-react';

/**
 *
 * @param {*} item
 */
export const getNameWithURL = (item) => (
  <div>
    {item.name}{' '}
    <a
      href={`https://${item.name.split(' ')[0]}.cern.ch`}
      title={item.name}
      target="_blank"
      rel="noopener noreferrer"
    >
      <Icon name="external" />
    </a>
  </div>
);

/**
 *
 * @param {*} item
 */
export const getLanDBLink = (item) => (
  <span>
    <Button
      as="a"
      href={`https://network.cern.ch/sc/fcgi/sc.fcgi?Action=SearchForDisplay&DeviceName=${
        item.name.split(' ')[0]
      }`}
      title={`${item.name} in Landb`}
      target="_blank"
      rel="noopener noreferrer"
      size="mini"
    >
      <Icon name="external" />
      Landb
    </Button>
  </span>
);

export const getStatusFormatter = (item) => {
  switch (item.status) {
    case 'I':
      return 'Supported';
    case 'IE':
      return 'Design';

    default:
      return item.status;
  }
};

export const getSourceFormatter = (item) => {
  let label;
  const customLabel = (
    <Label
      size="tiny"
      content="Custom"
      title="custom"
      color="orange"
      circular
    />
  );
  switch (item.source) {
    case 'infoream':
      label = (
        <Label
          size="tiny"
          content="Infoream"
          title="infoream"
          color="green"
          circular
        />
      );
      break;
    // case 'custom':
    //   label = <Label content="Custom" titlte="custom" color="orange" />;
    //   break;
    default:
      label = item.source;
  }
  if (item.custom) {
    label = (
      <>
        {label}<br/>
        <Popup
          trigger={customLabel}
          content={
            <ul>
              {Object.keys(item.custom).map((key, index) => (
                <li key={index}>
                  <strong>{key}</strong>: {item.custom[key]}
                </li>
              ))}
            </ul>
          }
          inverted
        />
      </>
    );
  }

  return label;
};

export const getRoomFormatter = (item) => {
  return <abbr title={item.position}>{item.room}</abbr>;
};
