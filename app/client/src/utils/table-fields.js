export const codecItemFields = [
  'source',
  'room',
  'name',
  'landb',
  'description',
  'status',
  'manufacturer',
  'model',
  'commissioned',
];
export const pcItemFields = [
  'source',
  'room',
  'name',
  'landb',
  'description',
  'status',
  'manufacturer',
  'model',
  'commissioned'
];
export const projectorItemFields = [
  'source',
  'room',
  'name',
  'landb',
  'description',
  'status',
  'manufacturer',
  'model',
  'commissioned'
];
export const loanItemFields = [
  'source',
  'category',
  'name',
  'description',
  'status',
  'manufacturer',
  'model',
  'serial_number'
];
export const partItemFields = [
  'source',
  'room',
  'name',
  'description',
  'base_price',
  'code',
  'quantity',
  'epa_code'
];

export const controllerItemFields = [
  'source',
  'room',
  'name',
  'landb',
  'description',
  'status',
  'manufacturer',
  'model',
  'commissioned',
];