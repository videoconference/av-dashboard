import { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  userLogout,
  userLogin,
  loginWindowOpened,
  loginWindowClosed
} from './actions';
import {
  getToken,
  getLoginWindowId,
  isLoggedIn,
  isAcquiringToken
} from './selectors/auth';
import config from './config';

export function useAuthentication() {
  const popup = useRef(null);
  const popupId = useRef(null);
  const loginWindowId = useSelector(getLoginWindowId);
  const isUserLoggedIn = useSelector(isLoggedIn);
  const acquiringToken = useSelector(isAcquiringToken);
  const dispatch = useDispatch();

  const login = () => {
    const width = window.outerWidth * 0.5;
    const height = window.outerHeight * 0.7;
    const windowLocation = {
      left: window.screen.availLeft + window.screen.availWidth / 2 - width / 2,
      top: window.screen.availTop + window.screen.availHeight / 2 - height / 2
    };
    if (popup.current) {
      popup.current.close();
    }
    popupId.current = Date.now();
    dispatch(loginWindowOpened(popupId.current));
    const hostname = window.location.protocol + '//' + window.location.host;
    const refererUrl = process.env.NODE_ENV === 'development'? `?referer_url=${hostname}` : '';
    popup.current = window.open(
      `${config.api.ENDPOINT}/login/${refererUrl}`,
      'login',
      `menubar=no,toolbar=no,location=no,dependent=yes,width=${width},height=${height},left=${windowLocation.left},top=${windowLocation.top}`
    );
  };

  const logout = () => {
    dispatch(userLogout());
  };

  useEffect(() => {
    const handleMessage = evt => {
      if (evt.source !== popup.current) {
        // ignore the message if it wasn't from our popup. this happens e.g. when this
        // hook is used in multiple places at the same time.
        return;
      }

      if (evt.origin !== window.origin) {
        // we should never get messages from different origins, so those are ignored too
        console.error(
          `Unexpected message origin: expected ${window.origin}, got ${evt.origin}`
        );
        if (process.env.NODE_ENV !== 'development') {
          return;
        }
      }
      if (evt.data.error) {
        console.warn(`Login failed: ${evt.data.error}`);
        return;
      }
      if (evt.data.token) {
        dispatch(userLogin(evt.data.token));
      }
    };

    const closePopup = () => {
      popupId.current = null;
      if (popup.current) {
        popup.current.close();
        popup.current = null;
      }
    };

    window.addEventListener('message', handleMessage);
    window.addEventListener('unload', closePopup);

    return () => {
      window.removeEventListener('unload', closePopup);
      window.removeEventListener('message', handleMessage);
      closePopup();
    };
  }, [dispatch]);

  useEffect(() => {
    if (isUserLoggedIn && !acquiringToken) {
      return;
    }
    const interval = window.setInterval(() => {
      if (
        popupId.current !== null &&
        loginWindowId === popupId.current &&
        (!popup.current || popup.current.closed)
      ) {
        dispatch(loginWindowClosed());
      }
    }, 250);

    return () => {
      window.clearInterval(interval);
    };
  }, [dispatch, loginWindowId, isUserLoggedIn, acquiringToken]);

  return { login, logout };
}

export function checkInitialToken(store) {
  const token = localStorage.getItem('token');
  if (token) {
    store.dispatch(userLogin(token));
  }
}

export function subscribeTokenChanges(store) {
  store.subscribe(() => {
    const token = getToken(store.getState());
    if (localStorage.getItem('token') === token) {
      return;
    }
    if (token) {
      localStorage.setItem('token', token);
    } else {
      localStorage.removeItem('token');
    }
  });
}
