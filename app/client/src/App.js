import React, { useState } from 'react';
import { useSelector } from 'react-redux';

import {
  BrowserRouter as Router,
  Route,
  Switch,
  NavLink
} from 'react-router-dom';
import { getErrors } from './selectors/errors';
import { isLoginWindowOpen } from './selectors/auth';

import './App.css';
import HomePage from './pages/HomePage';
import LoggingIn from './components/LoginIn';
import LoginPrompt from './components/LoginPrompt';

import {
  Container,
  Menu,
  Header,
  Icon,
  Message,
  Grid,
  Dropdown
} from 'semantic-ui-react';
import TopMenu from './components/TopMenu';
import config from './config';
import ConferenceRoomsPage from './pages/ConferenceRoomsPage';
import { getUserInfo } from './selectors/user';
import SettingsPage from './pages/SettingsPage';
import ApiKeyListPage from './pages/ApiKeyListPage';

/**
 * Main entry point
 */
function App() {
  const loggingIn = useSelector(isLoginWindowOpen);
  const errors = useSelector(getErrors);
  const user = useSelector(getUserInfo);

  return (
    <Router>
      <TopMenu />
      <Container className={`App ${config.IS_DEV_INSTALL ? 'paddedApp' : ''}`}>
        <Header as="h2">
          <Icon name="settings" />
          <Header.Content>
            Webcast & Videoconference Metadata Dashboard
            <Header.Subheader>
              Equipment of rooms, codecs and other devices
            </Header.Subheader>
          </Header.Content>
        </Header>
        <Menu>
          <Menu.Item as={NavLink} to={'/home'} name="home">
            <Icon name="home" />
            Home
          </Menu.Item>
          <Menu.Item as={NavLink} to={'/conference-rooms/rooms'} name="home">
            <Icon name="video" />
            Conference Rooms
          </Menu.Item>
          {user && user.roles.includes('admins') && (
            <>
              <Menu.Item as={NavLink} to={'/settings'} name="settings">
                <Icon name="settings" />
                Settings
              </Menu.Item>
              <Menu.Item as={NavLink} to={'/api-keys'} name="api-keys">
                <Icon name="key" />
                API Keys
              </Menu.Item>
              <Dropdown item text="External Monitoring">
                <Dropdown.Menu>
                  <Dropdown.Item
                    as="a"
                    href="https://es-collaborativeapps6.cern.ch/kibana/app/kibana#/dashboards?_g=h@44136fa&filter="
                    target="_blank"
                    title="Vidyo Kibana Stats"
                  >
                    <Icon name="external" /> Vidyo
                  </Dropdown.Item>
                  <Dropdown.Item
                    as="a"
                    href="https://cern.zoom.us/account/metrics/dashboard"
                    target="_blank"
                    title="Zoom Metrics Dashboard"
                  >
                    <Icon name="external" />
                    Zoom
                  </Dropdown.Item>
                  <Dropdown.Item
                    as="a"
                    href="https://avsys-monit.web.cern.ch/"
                    target="_blank"
                    title="AVSys Grafana Dashboard"
                  >
                    <Icon name="external" />
                    AVSys
                  </Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
              <Menu.Menu position="right">
                <Menu.Item
                  as="a"
                  href="/api/v1/"
                  target="_blank"
                  title="API Swagger Docs"
                >
                  API
                </Menu.Item>
                <Menu.Item
                  as="a"
                  href="https://dev-documentation.web.cern.ch/opsdoc/webcast-and-videoconference-metadata/"
                  target="_blank"
                  title="Dev Documentation"
                >
                  <Icon name="book" />
                </Menu.Item>
                <Menu.Item
                  as="a"
                  href="https://monit-timber-videoconference.cern.ch/kibana/app/kibana#/dashboards"
                  target="_blank"
                  title="App Monitoring"
                >
                  <Icon name="chart area" />
                </Menu.Item>
              </Menu.Menu>
            </>
          )}
        </Menu>

        <Grid>
          {errors.length > 0 && (
            <Grid.Row>
              <Grid.Column>
                {errors.map((error) => (
                  <div>
                    <DismissableMessage error={error} />
                  </div>
                ))}
              </Grid.Column>
            </Grid.Row>
          )}
        </Grid>

        <Switch>
          <Route exact path="/home" component={HomePage} />
          <Route exact path="/settings" component={SettingsPage} />
          <Route exact path="/api-keys" component={ApiKeyListPage} />
          <Route
            exact
            path="/conference-rooms/:id"
            component={ConferenceRoomsPage}
          />
          {/* Beware order matters */}
          <Route render={() => <div>This page does not exist</div>} />
        </Switch>
        <LoginPrompt />
        {loggingIn && <LoggingIn />}
      </Container>
    </Router>
  );
}

/**
 * Displays an error message that can be removed by the user clicking on the cross icon.
 * @param {Object} error - The error that will be displayed
 * @param {string} error.id - Error identifier
 * @param {string} error.error - Error message
 */
function DismissableMessage({ error }) {
  const [visible, setVisible] = useState(true);

  const hideMessage = () => {
    console.log('Click dismiss');
    setVisible(false);
  };

  if (visible) {
    return (
      <Message onDismiss={hideMessage} negative key={error.id} id={error.id}>
        {error.id}: {error.error}
      </Message>
    );
  }
  return null;
}

export default App;
