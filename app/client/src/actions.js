import config from './config';
import client from './client';

export const LOGIN_WINDOW_OPENED = 'Login window opened';
export const LOGIN_WINDOW_CLOSED = 'Login window closed';
export const LOGIN_PROMPT_ABORTED = 'User refused to login';
export const USER_LOGIN = 'User logged in';
export const USER_LOGOUT = 'User logged out';
export const USER_RECEIVED = 'User info received';
export const TOKEN_EXPIRED = 'Expired token needs refresh';
export const TOKEN_NEEDED = 'Login required to send request';

export const ADD_ERROR = 'Error occurred';
export const REMOVE_ERROR = 'Remove error';
export const CLEAR_ERRORS = 'Clear all the errors';

export function loginWindowOpened(id) {
  return { type: LOGIN_WINDOW_OPENED, id };
}

export function loginWindowClosed() {
  return { type: LOGIN_WINDOW_CLOSED };
}

export function loginPromptAborted() {
  return { type: LOGIN_PROMPT_ABORTED };
}

export function userLogin(token) {
  return async dispatch => {
    dispatch({ type: USER_LOGIN, token });
    dispatch(loadUser());
  };
}

export function userLogout() {
  return async dispatch => {
    dispatch({ type: USER_LOGOUT });
    const hostname = window.location.protocol + "//" + window.location.host
    window.location.href = `${config.api.ENDPOINT}/logout/?next_url=${hostname}`;
  };
}

export function loadUser() {
  return async dispatch => {
    const user = await client.catchErrors(client.getMe());
    if (user !== undefined) {
      dispatch({ type: USER_RECEIVED, user });
    }
    return user;
  };
}

export function tokenExpired() {
  return { type: TOKEN_EXPIRED };
}

export function tokenNeeded() {
  return { type: TOKEN_NEEDED };
}

export function addError(error) {
  return { type: ADD_ERROR, error };
}

export function removeError(errorId) {
  return { type: REMOVE_ERROR, id: errorId };
}

export function clearError() {
  return { type: CLEAR_ERRORS };
}
