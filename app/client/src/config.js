/**
 * Set the configuration on dev environments
 */
const dev = {
  api: {
    ENDPOINT: 'http://localhost:5000'
  },
  IS_DEV_INSTALL: process.env.REACT_APP_IS_DEV_INSTALL === 'true'
};
/**
 * Set the configuration for prod environments
 */
const prod = {
  api: {
    ENDPOINT: process.env.REACT_APP_API_ENDPOINT
  },
  IS_DEV_INSTALL: process.env.REACT_APP_IS_DEV_INSTALL === 'true'
};

/**
 * Set the configuration for test environments
 */
const test = {
  api: {
    ENDPOINT: 'http://127.0.0.1:5000'
  },
  IS_DEV_INSTALL: true
};

let tempConfig;

if (process.env.NODE_ENV === 'production') {
  tempConfig = prod;
}
if (process.env.NODE_ENV === 'development') {
  tempConfig = dev;
}
if (process.env.NODE_ENV === 'test') {
  tempConfig = test;
}
const config = tempConfig;

export default {
  // Add common config values here
  ...config
};
