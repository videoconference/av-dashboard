// auth
export const getToken = (state) => state.auth.token;
export const getLoginWindowId = (state) => state.auth.windowId;
export const isLoginWindowOpen = (state) => !!state.auth.windowId;
export const isLoggedIn = (state) => !!state.auth.token;
export const isAcquiringToken = (state) => !!state.auth.acquiringToken;
