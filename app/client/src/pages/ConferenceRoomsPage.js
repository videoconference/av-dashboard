import React, { useState, useEffect } from 'react';

import { Grid, Segment, Header, Menu } from 'semantic-ui-react';

import { useParams, useHistory } from 'react-router';
import { getUserInfo } from '../selectors/user';
import { useSelector } from 'react-redux';

import PrivateConferenceRoomsSection from '../components/ConferenceRooms/PrivateConferenceRoomsSection';
import PublicConferenceRoomsSection from '../components/ConferenceRooms/PublicConferenceRoomsSection';

/**
 * Conference Rooms Page
 */
export default function ConferenceRoomsPage() {
  const { id } = useParams();
  let history = useHistory();
  const user = useSelector(getUserInfo);

  const [activeItem, setActiveItem] = useState(id);

  useEffect(() => {
    setActiveItem(id);
  }, [id]);

  return (
    <Grid columns={1}>
      <Grid.Column>
        <Segment>
          <Header as="h3">Conference Rooms</Header>
          <Menu attached="top" tabular>
            <Menu.Item
              name="Rooms"
              active={activeItem === 'rooms'}
              onClick={() => history.push(`/conference-rooms/rooms`)}
            />
            <Menu.Item
              name="Loans"
              active={activeItem === 'loans'}
              onClick={() => history.push(`/conference-rooms/loans`)}
            />
            {user && user.roles.includes('admins') && (
              <>
                <Menu.Item
                  name="Codecs"
                  active={activeItem === 'codecs'}
                  onClick={() => history.push(`/conference-rooms/codecs`)}
                />
                <Menu.Item
                  name="Controllers"
                  active={activeItem === 'controllers'}
                  onClick={() => history.push(`/conference-rooms/controllers`)}
                />
                <Menu.Item
                  name="KPis"
                  active={activeItem === 'kpis'}
                  onClick={() => history.push(`/conference-rooms/kpis`)}
                />
                <Menu.Item
                  name="Parts"
                  active={activeItem === 'parts'}
                  onClick={() => history.push(`/conference-rooms/parts`)}
                />
                <Menu.Item
                  name="PCs"
                  active={activeItem === 'pcs'}
                  onClick={() => history.push(`/conference-rooms/pcs`)}
                />
                <Menu.Item
                  name="Projectors"
                  active={activeItem === 'projectors'}
                  onClick={() => history.push(`/conference-rooms/projectors`)}
                />
                <Menu.Item
                  name="Video Systems"
                  active={activeItem === 'video-systems'}
                  onClick={() => history.push(`/conference-rooms/video-systems`)}
                />
              </>
            )}
          </Menu>

          <Segment attached="bottom">
            <PublicConferenceRoomsSection activeItem={activeItem} user={user} />
            <PrivateConferenceRoomsSection
              activeItem={activeItem}
              user={user}
            />
          </Segment>
        </Segment>
      </Grid.Column>
    </Grid>
  );
}
