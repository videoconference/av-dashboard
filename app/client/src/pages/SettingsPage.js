import React from 'react';
import { Grid, Segment, Header } from 'semantic-ui-react';
import InvalidateCacheForm from '../components/SettingsPage/InvalidateCacheForm';
import FetchInforeamForm from '../components/SettingsPage/FetchInforeamForm';

export default function SettingsPage() {
  return (
    <Grid columns={1}>
      <Grid.Column>
        <Segment>
          <Header as="h3">Settings</Header>
            <InvalidateCacheForm/>
            <FetchInforeamForm/>
        </Segment>
      </Grid.Column>
    </Grid>
  );
}
