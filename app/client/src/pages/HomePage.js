import React from 'react';

import { Grid, Segment, Header } from 'semantic-ui-react';

/**
 * Homepage
 */
export default function HomePage() {

  return (
    <Grid columns={1}>
      <Grid.Column>
        <Segment>
          <Header as="h3">Home</Header>
          <p>Please, select an option on the top menu</p>
        </Segment>
      </Grid.Column>
    </Grid>
  );
}
