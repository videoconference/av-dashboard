import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { getApiKeys as getApiKeysSelector } from '../selectors/api_keys';
import { isLoggedIn } from '../selectors/auth';

import { Grid, Segment, Header, Table, Label } from 'semantic-ui-react';
import { getApiKeys } from '../actions/api_keys';
import { RevokeApiKeyForm } from '../components/ApiKeyListPage/RevokeApiKeyForm';
import AddApiKeyForm from '../components/ApiKeyListPage/AddApiKeyForm';
import RevealTokenButton from '../components/ApiKeyListPage/RevealTokenButton';


export default function ApiKeyListPage() {
  const dispatch = useDispatch();
  const apiKeys = useSelector(getApiKeysSelector);

  const isUserLoggedIn = useSelector(isLoggedIn);

  useEffect(() => {
    dispatch(getApiKeys());
  }, [dispatch]);

  if (!isUserLoggedIn) {
    return <Redirect to="/" />;
  }

  return (
    <Grid>
      <Grid.Row>
        <Grid.Column>
          <Segment>
            <Header as="h3">API Keys</Header>

            <Grid.Row>
              <Grid.Column>
                <AddApiKeyForm />
                <Table>
                  <Table.Header>
                    <Table.Row>
                      <Table.HeaderCell>Name</Table.HeaderCell>
                      <Table.HeaderCell>Api Key</Table.HeaderCell>
                      <Table.HeaderCell>Revoked</Table.HeaderCell>
                      <Table.HeaderCell>Revoke</Table.HeaderCell>
                    </Table.Row>
                  </Table.Header>
                  <Table.Body>
                    {apiKeys &&
                      apiKeys.map(apiKey => (
                        <Table.Row key={apiKey.id}>
                          <Table.Cell>{apiKey.name}</Table.Cell>
                          <Table.Cell singleLine={false}>
                            {`${apiKey.accessToken.substring(0, 50)}...`}{' '}
                            <RevealTokenButton apiKey={apiKey} />
                          </Table.Cell>
                          <Table.Cell>
                            {apiKey.revoked ? (
                              <Label color="red" content="Yes" />
                            ) : (
                              <Label color="green" content="No" />
                            )}
                          </Table.Cell>
                          <Table.Cell>
                            <RevokeApiKeyForm apiKey={apiKey} />
                          </Table.Cell>
                        </Table.Row>
                      ))}
                  </Table.Body>
                </Table>
              </Grid.Column>
            </Grid.Row>
          </Segment>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
}
