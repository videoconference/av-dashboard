function setUrlParam(uri, key, val) {
  return uri
    .replace(
      new RegExp('([?&]' + key + '(?=[=&#]|$)[^#&]*|(?=#|$))'),
      '&' + key + '=' + encodeURIComponent(val)
    )
    .replace(/^([^?&]+)&/, '$1?');
}

const metadataApiCalls = (state) => {
  return {
    getMetadata: (urlParams) => {
      let url = `${state.config.api.ENDPOINT}/api/private/v1/metadata/`;

      for (let index = 0; index < urlParams.length; index++) {
        const element = urlParams[index];
        url = setUrlParam(url, element[0], element[1]);
      }
      return state._request(url);
    },
    setMetadata: (urlParams, newData) => {
      let url = `${state.config.api.ENDPOINT}/api/private/v1/metadata/`;

      for (let index = 0; index < urlParams.length; index++) {
        const element = urlParams[index];
        url = setUrlParam(url, element[0], element[1]);
      }
      return state._request(url, {
        method: 'PUT',
        body: JSON.stringify(newData)
      });
    },
    clearMetadataCache: () => {
      let url = `${state.config.api.ENDPOINT}/api/private/v1/metadata-cache/`;
      return state._request(url, {
        method: 'POST'
      });
    },
    fetchInforeamData: (resourceType) => {
      let url = `${state.config.api.ENDPOINT}/api/private/v1/infoream-fetch/`;
      return state._request(url, {
        method: 'POST',
        body: JSON.stringify({resourceType})
      });
    }
  };
};

export default metadataApiCalls;
