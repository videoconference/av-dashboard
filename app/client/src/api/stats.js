const statsApiCalls = (state) => {
  return {
    getStats : (urlParams) => {
      let url = `${state.config.api.ENDPOINT}/api/private/v1/stats`

      return state._request(url);
    }
  };
};

export default statsApiCalls;