const userApiCalls = (state) => {
  return {
    getMe : () => {
      return state._request(`${state.config.api.ENDPOINT}/api/private/v1/users/me`);
    }
  };
};

export default userApiCalls;