const apiKeysApiCalls = state => {
  return {
    getApiKeys: () => {
      return state._request(`${state.config.api.ENDPOINT}/api/private/v1/api-keys/`);
    },
    revokeApiKey: id => {
      return state._request(
        `${state.config.api.ENDPOINT}/api/private/v1/api-keys/${encodeURI(id)}`,
        {
          method: 'DELETE'
        }
      );
    },
    addNewApiKey: details => {
      console.log(details);
      return state._request(`${state.config.api.ENDPOINT}/api/private/v1/api-keys/`, {
        method: 'POST',
        body: JSON.stringify(details)
      });
    }
  };
};

export default apiKeysApiCalls;
