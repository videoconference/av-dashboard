import client from "../client";

export const API_KEYS_RECEIVED = 'List of API keys received';
export const API_KEY_REVOKED = 'API Key has been revoked';
export const ADD_API_KEY_SUCCESS = 'API Key has been added'

export function getApiKeys() {
  return async dispatch => {
    const apiKeys = await client.catchErrors(client.getApiKeys());
    if (apiKeys !== undefined) {
      dispatch({ type: API_KEYS_RECEIVED, apiKeys });
    }
    return apiKeys;
  };
}

export function addNewApiKey(details) {
  console.log(details)
  return async dispatch => {
    const result = await client.catchErrors(client.addNewApiKey(details));
    console.log(result);
    if (result !== undefined) {
      dispatch({ type: ADD_API_KEY_SUCCESS, result });
    }
    return result;
  };
}

export function revokeApiKey(id) {
  console.log(id)
  return async dispatch => {
    const result = await client.catchErrors(client.revokeApiKey(id));
    console.log(result);
    if (result !== undefined) {
      dispatch({ type: API_KEY_REVOKED });
    }
    return 'ok';
  };
}