import client from "../client";

export const METADATA_RECEIVED = 'Metadata received';
export const METADATA_SET = 'Metadata set';
export const CACHE_CLEARED = 'Metadata cache cleared';
export const INFOREAM_FETCHED = 'Data fetched from Infoream';



export function getMetadata(urlParams) {
  return async dispatch => {
    const metadata = await client.catchErrors(client.getMetadata(urlParams));
    if (metadata !== undefined) {
      dispatch({ type: METADATA_RECEIVED, metadata });
    }
    return metadata;
  };
}

export function setMetadata(urlParams, newData) {
  return async dispatch => {
    const metadata = await client.catchErrors(client.setMetadata(urlParams, newData));
    if (metadata !== undefined) {
      dispatch({ type: METADATA_SET, metadata });
    }
    return metadata;
  };
}

export function clearMetadataCache() {
  return async dispatch => {
    const result = await client.catchErrors(client.clearMetadataCache());
    if (result !== undefined) {
      dispatch({ type: CACHE_CLEARED, result });
    }
    return result;
  };
}

export function fetchInforeamData(resourceType) {
  return async dispatch => {
    const result = await client.catchErrors(client.fetchInforeamData(resourceType));
    if (result !== undefined) {
      dispatch({ type: INFOREAM_FETCHED, result });
    }
    return result;
  };
}