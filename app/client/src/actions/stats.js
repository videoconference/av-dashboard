import client from "../client";

export const STATS_RECEIVED = 'Stats received';


export function getStats() {
  return async dispatch => {
    const stats = await client.catchErrors(client.getStats());
    if (stats !== undefined) {
      dispatch({ type: STATS_RECEIVED, stats });
    }
    return stats;
  };
}