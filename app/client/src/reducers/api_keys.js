import {
  API_KEYS_RECEIVED,
} from '../actions/api_keys';

const INITIAL_STATE = {
  apiKeys: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case API_KEYS_RECEIVED:
      console.log(action);
      return {
        ...state,
        apiKeys: action.apiKeys
      };
    default:
      return state;
  }
};