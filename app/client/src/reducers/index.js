import { combineReducers } from 'redux';
import auth from './auth';
import user from './user';
import metadataReducer from './metadata';
import apiKeysReducer from './api_keys';



import { ADD_ERROR, REMOVE_ERROR, CLEAR_ERRORS } from '../actions';

export default combineReducers({
  auth,
  user,
  apiKeys: apiKeysReducer,
  metadata: metadataReducer,
  error: (state = [], action) => {
    switch (action.type) {
      case ADD_ERROR: {
        const id = state.length !== 0 ? state[state.length - 1].id + 1 : 0;
        return [...state, { id, error: action.error }];
      }
      case REMOVE_ERROR:
        return state.filter((error) => error.id !== action.id);
      case CLEAR_ERRORS:
        return [];
      default:
        return state;
    }
  }
});
