import {
  METADATA_RECEIVED
} from '../actions/metadata';

const INITIAL_STATE = {
  metadata: {},
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case METADATA_RECEIVED:
      return {
        ...state,
        metadata: action.metadata
      };
    default:
      return state;
  }
};