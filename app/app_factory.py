import os
from importlib import import_module

from flask import Flask, render_template
from flask_cors import CORS
from werkzeug.middleware.proxy_fix import ProxyFix

from app.api_loader import (load_api_private_blueprints, load_api_public_blueprints)
from app.extensions import cache, db, migrate, jwt
from app.utils.auth.blueprints import auth
from app.utils.auth.oidc import oauth
from app.utils.blacklist_helpers import check_if_token_revoked
from app.utils.cli import initialize_cli
from app.utils.logger.logger import setup_logs


def create_app(config):
    print('Creating Flask application...', end=' ')
    app = Flask(__name__)
    app.config.from_object(config)
    print("ok")

    print('Initializing logs and cli...', end=' ')
    setup_logs(app, 'webapp', to_remote=app.config.get('LOG_REMOTE_ENABLED', False))
    setup_logs(app, 'job', to_remote=app.config.get('LOG_REMOTE_ENABLED', False))
    initialize_cli(app)
    print("ok")

    print('Adding CORS to the application...', end=' ')
    CORS(app)
    print("ok")

    print('Initializing CLI...', end=' ')
    initialize_cli(app)
    print("ok")

    print('Adding proxy to the application', end=' ')
    if app.config['USE_PROXY']:
        app.wsgi_app = ProxyFix(app.wsgi_app, x_for=1, x_proto=1, x_host=1)
    print("ok")

    print('Initialize Oauth and OpenID', end=' ')
    oauth.init_app(app)
    app.register_blueprint(auth)
    print("ok")

    setup_database(app)

    initialize_models(app)

    init_jwt_auth(app)

    """
    Initialize Cache
    """
    init_redis_cache(app)

    load_private_api_v1(app)

    load_public_api_v1(app)

    @app.route('/')
    def home():
        data = {}
        return render_template('home.html', metadata=data)
    return app


def load_private_api_v1(application):
    print('Loading private API blueprints...', end=' ')
    all_blueprints = load_api_private_blueprints()

    for bp in all_blueprints:
        application.register_blueprint(
            bp, url_prefix='{prefix}/private/{version}'.format(prefix='/api', version='v1'))
    print("ok")


def load_public_api_v1(application):
    print('Loading public API blueprints...', end=' ')
    all_blueprints = load_api_public_blueprints()

    for bp in all_blueprints:
        application.register_blueprint(
            bp, url_prefix='{prefix}/{version}'.format(prefix='/api', version='v1'))
    print("ok")


def init_redis_cache(app):
    config = {'CACHE_TYPE': 'null'}
    if app.config.get('CACHE_ENABLE', False):
        print("Initializing Redis Caching... ", end='')
        config = {'CACHE_TYPE': 'redis',
                  "CACHE_REDIS_HOST": app.config.get('CACHE_REDIS_HOST', None),
                  "CACHE_REDIS_PASSWORD": app.config.get('CACHE_REDIS_PASSWORD', None),
                  "CACHE_REDIS_PORT": app.config.get('CACHE_REDIS_PORT', None)

                  }
    else:
        print("Caching disabled... ", end='')
    cache.init_app(app, config=config)
    print("ok")


def setup_database(app):
    """
    Initialize the database and the migrations
    """

    if app.config.get("DB_ENGINE", None) == "postgresql":
        print("Initializing Postgres Database... ", end='')
        app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(
            app.config["DB_USER"],
            app.config["DB_PASS"],
            app.config["DB_SERVICE_NAME"],
            app.config["DB_PORT"],
            app.config["DB_NAME"]
        )

        app.config["SQLALCHEMY_RECORD_QUERIES"] = False
        app.config["SQLALCHEMY_POOL_SIZE"] = int(app.config.get('SQLALCHEMY_POOL_SIZE', 5))
        app.config["SQLALCHEMY_POOL_TIMEOUT"] = int(app.config.get('SQLALCHEMY_POOL_TIMEOUT', 10))
        app.config["SQLALCHEMY_POOL_RECYCLE"] = int(app.config.get('SQLALCHEMY_POOL_RECYCLE', 120))
        app.config["SQLALCHEMY_MAX_OVERFLOW"] = int(app.config.get('SQLALCHEMY_MAX_OVERFLOW', 3))

    elif app.config.get("DB_ENGINE", None) == "mysql":
        print("Initializing Mysql Database... ", end='')
        app.config["SQLALCHEMY_DATABASE_URI"] = 'mysql+pymysql://{0}:{1}@{2}:{3}/{4}?charset=utf8mb4'.format(
            app.config["DB_USER"],
            app.config["DB_PASS"],
            app.config["DB_SERVICE_NAME"],
            app.config["DB_PORT"],
            app.config["DB_NAME"]
        )

    else:
        print("Initializing Sqlite Database...", end='')
        _basedir = os.path.abspath(os.path.dirname(__file__))
        app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite:///' + os.path.join(_basedir, 'webapp.db')

    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

    db.init_app(app)
    db.app = app
    print("ok")

    print("Initializing Database Migrations... ", end='')
    migrate.init_app(app, db)
    print("ok")


def initialize_models(app):
    """
    Load all the available models
    """
    with app.app_context():
        print("Loading models... ", end='')
        db_models_imports = [
            'app.models.token_blacklist'
        ]
        for module in db_models_imports:
            import_module(module)
        print("ok")


def init_jwt_auth(app):
    """
    Initializes the JWT Authentication and sets the global configuration for the application JWT
    :param app: Application that will be set up
    :return: -
    """
    print("Initializing JWT Authentication... ", end='')
    app.config['JWT_ACCESS_TOKEN_EXPIRES'] = False
    app.config['JWT_BLACKLIST_ENABLED'] = True
    app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']

    jwt.init_app(app)

    jwt.token_in_blacklist_loader(check_if_token_revoked)
    print("ok")
