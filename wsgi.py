from app.app_factory import create_app
from secret import config

application = create_app(config)
