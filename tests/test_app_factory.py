import unittest

from flask import url_for

from app.app_factory import create_app
from tests import config_test
from tests.base import BaseTestCase


class AppFactoryTest(unittest.TestCase):

    def test_app_factory(self):
        config = config_test
        app = create_app(config)

        self.assertTrue(app)


class HomeViewTest(BaseTestCase):
    def test_home(self):
        with self.client:
            result = self.client.get(url_for('home'))
            #
            self.assertEqual(result.status_code, 200)
            self.assertIn('This is the home', str(result.data))
