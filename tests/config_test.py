SECRET_KEY = '&\xb8\x1eC\x81\xfcb\x13/\x80\x14OEJ\xd5\x92lM\xf7k\x9f\x9ct '
USE_PROXY = True

########################################################
# Logging configuration
########################################################
LOG_LEVEL = "DEV"
LOG_REMOTE_ENABLED = False
LOG_REMOTE_PRODUCER = "vc-metadata"
LOG_REMOTE_TYPE = "vc-metadata-test"

########################################################
# Gitlab configuration
########################################################
GITLAB_ACCESS_TOKEN = "abcdefghi"
METADATA_REPO_URL = "https://gitlab.cern.ch/webcast/metadata-test.git"
BRANCH = 'test_test'
########################################################
# OIDC configuration
########################################################
#
SKIP_LOGIN = True
# How long a login token is valid.
TOKEN_LIFETIME = 12 * 3600
# OIDC Parameters
OIDC_CLIENT_ID = "test"
OIDC_JWKS_URL = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/certs"
OIDC_ISSUER = "https://auth.cern.ch/auth/realms/cern"
OIDC_LOGOUT_URL = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout"
# The client secret assigned to the application by your OIDC provider
OIDC_CLIENT_SECRET = "11111-2222-33333-444444"
# The authorization URL of the OIDC provider.
# autoconfig: the value of .authorization_endpoint
OIDC_AUTHORIZE_URL = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth"
# The token URL of the OIDC provider.
# autoconfig: the value of .token_endpoint
OIDC_ACCESS_TOKEN_URL = "https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token"
# The additional arguments used in the oauth client.
OIDC_CLIENT_KWARGS = {
    # autoconfig: a valid from .token_endpoint_auth_methods_supported
    'token_endpoint_auth_method': 'client_secret_post',
    'scope': 'openid profile email',
}

########################################################
# ORACLE configuration
########################################################
ORACLE_USER = '<NO_USER>'
ORACLE_PASSWORD = '<NO_PASSWORD>'
ORACLE_HOST = 'oracle-db.test.cern.ch'
ORACLE_PORT = '1111'
ORACLE_SERVICE = 'oracle-db-service.test.cern.ch'

########################################################
# CACHING Configuration: Flask Caching
########################################################
CACHE_ENABLE = False
CACHE_REDIS_PASSWORD = ""
CACHE_REDIS_HOST = "127.0.0.1"
CACHE_REDIS_PORT = "6379"
