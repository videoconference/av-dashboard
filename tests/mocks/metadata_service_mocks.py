from httmock import urlmatch


@urlmatch(netloc=r'(.*\.)?gitlab\.cern\.ch$')
def gitlab_get_file_mock(url, request):
    return {'status_code': 200, 'content': {'hello': 'world', 'last_commit_id': '123456'}}


@urlmatch(netloc=r'(.*\.)?gitlab\.cern\.ch$')
def gitlab_get_file_mock_data_empty(url, request):
    return {'status_code': 200, 'content': {}}


@urlmatch(netloc=r'(.*\.)?gitlab\.cern\.ch$')
def gitlab_get_file_mock_no_data(url, request):
    return {'status_code': 200}
