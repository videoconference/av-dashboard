from httmock import urlmatch


@urlmatch(netloc=r'(.*\.)?gitlab\.cern\.ch$')
def gitlab_get_file_mock_error(url, request):
    return {'status_code': 400}


@urlmatch(netloc=r'(.*\.)?gitlab\.cern\.ch$')
def gitlab_get_file_mock_success(url, request):
    return {'status_code': 200, 'content': {'hello': 'world'}}


@urlmatch(netloc=r'(.*\.)?gitlab\.cern\.ch$')
def gitlab_set_file_mock_error(url, request):
    return {'status_code': 400}


@urlmatch(netloc=r'(.*\.)?gitlab\.cern\.ch$')
def gitlab_set_file_mock_success(url, request):
    return {'status_code': 200, 'content': {'hello': 'world'}}