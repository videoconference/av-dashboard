from flask import session, g
from httmock import HTTMock

from app.services.metadata_service import MetadataClient, MetadataClientError, get_metadata_client
from tests.base import BaseTestCase
from tests.mocks.metadata_service_mocks import gitlab_get_file_mock, gitlab_get_file_mock_no_data, \
    gitlab_get_file_mock_data_empty


class MetadataServiceTest(BaseTestCase):
    branch = 'test_test'
    user = {'email': 'test@cern.ch', 'first_name': 'James', 'last_name': 'Holden'}
    data = {}
    message = 'This is a custom message'

    gitlab_project_id = '12345'
    file_path = 'data.json'
    gitlab_access_token = 'abcdefg'

    def test_create_metadata_client(self):
        with HTTMock(gitlab_get_file_mock):
            client = MetadataClient("codec", branch=self.branch)
            self.assertIsNotNone(client)

    def test_create_metadata_client_verify_files(self):
        with HTTMock(gitlab_get_file_mock):
            client = MetadataClient("codec", branch=self.branch)
            self.assertEqual(client.files, ["codec_infoream.json", "codec_custom.json"])

    def test_create_metadata_client_verify_branch(self):
        with HTTMock(gitlab_get_file_mock):
            client = MetadataClient("codec", branch=self.branch)
            self.assertEqual(client.branch, self.branch)

    def test_create_metadata_client_verify_metadata(self):
        with HTTMock(gitlab_get_file_mock):
            client = MetadataClient("codec", branch=self.branch)
            self.assertEqual(client.metadata,
                             {'codec_custom': {'hello': 'world', 'last_commit_id': '123456'},
                              'codec_infoream': {'hello': 'world', 'last_commit_id': '123456'}})

    def test_create_metadata_client_no_branch(self):
        with HTTMock(gitlab_get_file_mock):
            with self.assertRaises(MetadataClientError):
                MetadataClient("codec")

    def test_create_metadata_client_factory_success(self):
        with HTTMock(gitlab_get_file_mock):
            client = get_metadata_client("codec")
            self.assertIsNotNone(client)

    def test_create_metadata_client_factory_gitlab_no_data(self):
        with HTTMock(gitlab_get_file_mock_no_data):
            client = get_metadata_client("codec")
            self.assertIsNotNone(client)

    def test_create_metadata_client_factory_gitlab_data_empty(self):
        with HTTMock(gitlab_get_file_mock_data_empty):
            client = get_metadata_client("codec")
            self.assertIsNotNone(client)

    def test_get_current_commit_id(self):
        with HTTMock(gitlab_get_file_mock):
            client = get_metadata_client("codec")
            last_commit_id = client.get_current_commit_id("codec")
            self.assertIsNotNone(last_commit_id)

    def test_get_current_commit_id_no_custom_key(self):
        with HTTMock(gitlab_get_file_mock):
            client = get_metadata_client("codec")
            client.metadata["codec_custom"] = None
            with self.assertRaises(MetadataClientError):
                client.get_current_commit_id("codec")

    def test_get_remote_commit_id(self):
        with HTTMock(gitlab_get_file_mock):
            client = get_metadata_client("codec")
            last_commit_id = client.get_remote_commit_id("codec")
            self.assertIsNotNone(last_commit_id)

    def test_set_metadata_to_gitlab(self):
        with HTTMock(gitlab_get_file_mock):
            client = get_metadata_client("codec")
            new_data = {'hello': 'world'}
            g.user = self.user
            last_commit_id = client.set_metadata_to_gitlab(new_data, "codec", self.branch)
            self.assertEqual(last_commit_id, {'hello': 'world', 'last_commit_id': '123456'})

    def test_set_metadata_to_gitlab_different_commits(self):
        with HTTMock(gitlab_get_file_mock):
            client = get_metadata_client("codec")
            new_data = {'hello': 'world'}
            g.user = self.user
            client.metadata["codec_custom"]["last_commit_id"] = '22222'
            result = client.set_metadata_to_gitlab(new_data, "codec", self.branch)
            self.assertEqual(result, False)

