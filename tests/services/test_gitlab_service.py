from flask import g

from app.services.gitlab_service import (build_request_data, GitlabClient, GitlabClientException)
from httmock import HTTMock

from tests.base import BaseTestCase
from tests.mocks.gitlab_service_mocks import (gitlab_get_file_mock_error, gitlab_get_file_mock_success, \
                                              gitlab_set_file_mock_error, gitlab_set_file_mock_success)


class GitlabServiceTest(BaseTestCase):
    branch = 'qa'
    user = {'email': 'test@cern.ch', 'first_name': 'James', 'last_name': 'Holden'}
    data = {}
    message = 'This is a custom message'

    gitlab_project_id = '12345'
    file_path = 'data.json'
    gitlab_access_token = 'abcdefg'

    def test_build_request_data_default_message(self):
        result = build_request_data(self.data, self.branch, self.user)

        self.assertEqual(result['branch'], self.branch)
        self.assertEqual(result['author_email'], self.user['email'])
        self.assertEqual(result['content'], self.data)
        self.assertEqual(result['commit_message'], 'Update the data file')
        self.assertEqual(result['author_name'], self.user['first_name'] + ' ' + self.user['last_name'])

    def test_build_request_data_custom_message(self):
        result = build_request_data(self.data, self.branch, self.user, self.message)

        self.assertEqual(result['branch'], self.branch)
        self.assertEqual(result['author_email'], self.user['email'])
        self.assertEqual(result['content'], self.data)
        self.assertEqual(result['commit_message'], self.message)
        self.assertEqual(result['author_name'], self.user['first_name'] + ' ' + self.user['last_name'])

    def test_build_request_data_no_user(self):
        g.user = {}
        self.assertRaises(GitlabClientException, build_request_data, self.data, self.branch, message=self.message)

    def test_gitlab_client_constructor(self):
        client = GitlabClient(self.gitlab_project_id, self.file_path, self.branch, self.gitlab_access_token)
        self.assertEqual(client.repo_url, 'https://gitlab.cern.ch')
        self.assertEqual(client.api_get_path,
                         '/api/v4/projects/{}/repository/files/{}?ref={}'.format(self.gitlab_project_id, self.file_path,
                                                                                 self.branch))
        self.assertEqual(client.api_set_path,
                         '/api/v4/projects/{}/repository/files/{}'.format(self.gitlab_project_id, self.file_path))
        self.assertEqual(client.branch, self.branch)

        headers = {
            'PRIVATE-TOKEN': self.gitlab_access_token,
            "Content-Type": "application/json"
        }
        self.assertEqual(client.headers, headers)

    def test_gitlab_client_constructor_no_token(self):
        client = GitlabClient(self.gitlab_project_id, self.file_path, self.branch)
        self.assertIsNotNone(client)

    def test_gitlab_client_get_file_error(self):
        client = GitlabClient(self.gitlab_project_id, self.file_path, self.branch, self.gitlab_access_token)

        with HTTMock(gitlab_get_file_mock_error):
            result = client.get_file()
            self.assertEqual(result, False)

    def test_gitlab_client_get_file_success(self):
        client = GitlabClient(self.gitlab_project_id, self.file_path, self.branch, self.gitlab_access_token)

        with HTTMock(gitlab_get_file_mock_success):
            result = client.get_file()
            self.assertEqual(result, {'hello': 'world'})

    def test_gitlab_client_set_file_error(self):
        client = GitlabClient(self.gitlab_project_id, self.file_path, self.branch, self.gitlab_access_token)

        with HTTMock(gitlab_set_file_mock_error):
            result = client.set_file(self.data, self.message, self.user)
            self.assertEqual(result, False)

    def test_gitlab_client_set_file_success(self):
        client = GitlabClient(self.gitlab_project_id, self.file_path, self.branch, self.gitlab_access_token)

        with HTTMock(gitlab_set_file_mock_success):
            result = client.set_file(self.data, self.message, self.user)
            self.assertEqual(result, {'hello': 'world'})
