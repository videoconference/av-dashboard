FROM centos/python-36-centos7

EXPOSE 8080

# Install yum packages with the ROOT user
USER root

COPY repo/centos-cernonly.repo /etc/yum.repos.d

#RUN yum update -y && yum -y install epel-release
RUN yum -y install oracle-instantclient-devel


ENV LD_LIBRARY_PATH /opt/rh/rh-python36/root/usr/lib64:/opt/rh/httpd24/root/usr/lib64
ENV PYTHONPATH /opt/app-root/src
ENV FLASK_APP /opt/app-root/src/wsgi.py

#RUN yum update -y && \
#    yum install -y \
#    https://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/Packages/java-1.8.0-oracle-src-1.8.0.152-2.el7.cern.x86_64.rpm
#    https://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/Packages/CERN-CA-certs-20180516-1.el7.cern.noarch.rpm



# Set the permissions for the app-user user
RUN chgrp -R 0 /opt/app-root && chmod -R ug+rwx /opt/app-root

RUN pip install --upgrade pip

USER 1001

WORKDIR /opt/app-root/src

# Install pip requirements
COPY requirements.txt /opt/app-root/src/requirements.txt
RUN pip install -r requirements.txt

# Copy the application files
COPY app /opt/app-root/src/app
COPY etc /opt/app-root/src/etc
COPY secret /opt/app-root/src/secret
COPY server /opt/app-root/src/server
COPY wsgi.py /opt/app-root/src/wsgi.py

RUN which pip
RUN which python
RUN which python3

RUN python --version
RUN pip --version

CMD ["/opt/app-root/src/etc/entrypoint.sh"]
